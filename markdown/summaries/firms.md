---
output: 'html\_notebook'
title: Neoclassical cost structure of a firm
---

# Cost structure of a neoclassical firm

In the neoclassical theory of the firm there is no internal structure
hierarchy or institutional behavior, just a number of continues cost and
benefit functions and their integrals and derivatives. The firm is a
profit maximizing creature faced with marginal revenue dictated by the
market, fixed costs and marginal costs. In the long run firms are
assumed to pick the amount to produce which maximizes profits and the
combination of factors of production which minimize the cost of
production for that level of output. This profit maximizing point is
given by maximizing profits with respect to quantity where profits are
given by the difference between revenue and price. For a price taking
firm marginal revenue is equel to the market price and profits will be
maximized by producing up until marginal costs exceed marginal revnue.
This allows us to treat the upward sloping part of the firms MR curve as
the supply curve for the firm and a sum of these as the supply curve for
the industry. This implies that all firms in the industry will operate
at the same marginal cost.

However, different firms may have different internal cost structures and
some may yeild profits that can be accounted for as rents on rare
technology or natural resources they own that cannot be replicated. The
defining feature for this market to represent an efficent allocation is
that no firm new or existing in the market may expand production at a
marginal cost below the market price. The neoclassical theory assumes
that at some point costs for a firm will begin to rise with quantity
produced this will constrain the profitable production a firm can engage
in. These decreasing returns to scale takes place at the level of the
firm on not the industry in perfect competition models otherwise another
firm could produce at additional output below the cost of the first
firm. These means that the cost structures of firms ensure many firms
will supply the industry. In addition it guarntees that the marginal
cost of any firm in the industry is not above the minium possible cost
for a new entrant, (the quantity at which marginal cost is equel to
averge cost for a new entrant), this ensures some measure of productive
efficency as firms unable to match these costs will not survive in the
long run. Products are consumed up until marginal demand is equal to
margin cost. This yields allocative efficiency though an allocative
efficeny in favour of the interests of the monied classes.

One of the papers (scissors does mentions increasing resource costs as a
reason why this may couse firm size to not increase but this violates
other assumptions possibly?

Pigou and The other classical economists give various failed
explanations for upward sloping supply curves.

-   Marginal productive of various short term factors of production
    decline (Notably labour) If all other variables are hell constant.

-   At larger scales coordination failures are heralded as problems
    though this may lake grounding.

-   Increased resource costs means firms influence other markets and
    also does not limit firms scope as the same increased resource costs
    would face a competitor.

-   Do firms underinvest or deplete resources in completion and can
    individual firms afford not to?

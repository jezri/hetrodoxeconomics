# Imports

import package: (adeHonours) document: (monopoly) as: (m)

# Main

## Question

Question:

: In light of its historical, political and economic origins, it can be
said that De Beers Conforms to the notion of a ‘natural monopoly’. Discuss this statement
with reference to the neoclassical theory of monopoly, especially in relation to the pricing
practices of De Beers.

## Answer

**NO. Its not a natural monopoly**
<!--
* De Beers is an artificial not a natural monopoly.
* Small barries to entry and low fixed costs naturally.
* Small scale diamond miners and alluviail diamonds were common.
* Political barriers to entry created by De Beers have kept the cartel.
* We need a theory of monopoly based on political influence (ignored by noeclassical)
    * Very little of the monopoly profits went to workers with many workers in Africa and India earning incredibly low wages and working in very poor conditions.
-->
    
## Neoclassical theory of monopoly

### ref(m:Monopoly;Monopoly)

## Neoclassical theory of natural monopoly

### ref(m:Natural Monopoly)

## Demand for diamonds

**Neoclassical model**:

:Supply and demand are separate

**Truth**:

:Advertising influence consumer preference + (Monopoly profit funds advertising)

---

**"Diamonds are forever"**:

: Stopped resale. So changing supply.

---

* Price is the reason for demand
* Commodification of commitment and relationships
    * Pricing peeks at discrete levels
* Is this entirely manufactured demand by De Beers?
* Do gender norms and societal ideas make society demand a similar good even if it wasn't diamonds?
* Do the two enforce each other?



## De Beers history

### ~~Monopoly~~ Perfect competition

* Digging in South Africa began as a competitive market
* Limited size and number of claims
* No wage labour (Minimal land expropriation until later)

### Wage labour

* As depth increased labour demand increased
* Low supply meant wages rose

### Contradictions of perfect completion

* Profits dropped
* No one could afford not to sell or to buy when demand was low.
* Numerous recessions and a stock market collapse removed small scale miners and dealers.


### Consolidation under De Beers

* Legal barriers were overturned
* Joint stoke capital intensive mining was started
* Supply was reduced
* Workers were layed of
* Wages and working conditions plummeted

<!--
* From the 1860s onwards very large diamond reserves were disovered in South Africa quickly becoming the largest supply global. 
* High competion both amoung miners and traders drove down profits and led to price volatility
* Low profits made traders and miners unwilling to accumulate stock. 


There were numerous barries to concentration of the industry

* Instatutions set up by early miners limited the amouont of mining claims an individual could hold.
* The labour force willing to work as wage labourers was limited.
    * Most white workers had sufficient capital to act as independent prospectors.
    * African communities still survived of subsistance agriculture.
    
Supply and demand for wage labour began to increase

* Supply increased due to dispossesion both 
  * Of agricultural land
  * Of mining claims from black miners
  * Futher new taxe s forced african workers to seek wage labour
  
* Demand increased as the deaper the pits got the more difficult they were to mine independently.


---

However by the 1870s labour was still in relitivly short supply (with much land disposetion still to come) wages were high profits were low and technical diffuculties invoved in deaper mining were arising.

### Consintration of mining right

* By the 1880 after a global financial crises and a revolt by miners limititions and the number of claims miners could hold were abandoed.

* Joint stock componies formed with De Beers as the largest

* Mechanization began to take place

* Labours bargaining power began to weeken and De Beers introduced convist labour in a system which would later be reimplemented with "voluntary labour"

### De Beer gains market dominance 1886 

* A stock market crash removed many smaller miners.
* Increased productivity led to growing output risked destroying the market for diamonds.
* Increasing difficulty and the end of open pit mining further reduced competion
* Significant market power was gained by De Beers and retrechment began in earnest to reduce output.
* The massive reduction in labour deamnd reduced wages and allowed for the introduction of the compound system.

---

* De Beers began collusion with a number of other large daimond mining componoies in South Africa to limit supply to the world market.
* De Beers maintained this monopoly by buying interests in all other firms as well as buying new diamond fields

-->

### Early 1900

* Two other major diamond companies still existed independently of De Beers
* There was another recession and diamond prices began to fall.
* Output quotes for the different companies was agreed upon
* The De Beers corporation became deeply invested in other areas of the South African economy

### Activities since

De beers syndicate later managed to protect its monopoly status. Even though many diamond field were discovered in Botswana, Australia and Russia and other African countries. This was done through massive political influence and the threat of damaging the market for new entrance.

  



## Another theory of monopoly

In order to explain the monopoly of De Beers we need a theory of capitalism outside of the neoclassical theories monopoly and natural monopoly. This theory must include long term defensible profits, manufactured demand and political influence. 

### Game thoery and defensible profits

De beers can defend against compeitors by maintianing a threat to flood the market. They have emence reserve capitcity (Diamonds being easy to store), and can easily flood sub markets by selling diamonds matching those of a new suppliers. There would be no point in a new supplier flooding the market and displacing De Beers cartel diamonds are actually abundent and the new competitor would destory the value of diamonds forever. Thus there is little incentive not to collude and join with De Beers.

## hide(Comment on reading)

**Is the Cato institute not a fairly right wing think tank? The paper makes some interesting ethical claims about how redistrabution is only disrible to give everyone a basic standard of living and anything above that is more painful to the wealthy being redistruted from than the poor being redistrubted to. The paper litterally says that we shouldn't regulate monopolies becouse inequilty between billionares and the middle class is disirable, like wow talk about nominitive economics**

**Futher the entire article contains many attempts to equate competitve markets with monopoly's in ways which really on many unsupported and improbable assumptions based on the presupped existance of perfect and perfectly informed markets elsewhere. For example there is a claim that inovation and research is easy to finace by profitless small firms through cridit, when such R & D is by definition extremely risky and riddeled with fundemental uncertainty. One would have to assume lenders with incredible knoweldge of the likely success and impact of inovation for this to be the case**


**There also seems a bazaar contradiction in the extent to which knowledge and competence are possible for different groups, while consumers are easily able to assess quality at a glance beauracts performing an investigation on the firm are incapable of doing so without great difficulty. Potential competitors are presumably easily able to see if exesive profits arise from an industry and thus try and enter it, but let a regulator try and determine this and they will hit a brick wall**

**I honestly think this reading should be ditched and replaced with a slight more nuanced right wing take**

**The paper is oddly transparent about the political power corporations have (Considering it being written by an institution founded by the Koch brothers), but seems blind to the issues this can cause. Also it just argues for rent controls somewhere in the middle which seems like a bit of a break in tone**

**The paper treats regulation to limit access and anti monopoly legislation as made by one large unified though confused government body. However this seems unlikely as the two serve very different interest groups, it seems more likely that government sometimes acts corruptly for special interests and sometime in an attempt to quash monopolies.**

**They also seem to play a game of willful ignorance constantly pitting a government policy against the market structure least suited to it. For example criticizing profit controls on a market that is not a natural monopoly**

**There is another common neo-liberal tactic hear. Break government agencies and than say that the broken agency is broken and must therefor be removed entirely. For example they claim that government agencies cannot tell if a monopoly company is running inefficiently. Maybe they can't if they have to go through a massive formal procedure and then have an extremely limited range of policy options available to them. However if they can without massive political push back enforce that some new technology must be adopted by a firm without having to prove such in restictive procedures they may be more useful, off course this may not be the case**


**They want to just use anti trust laws to regulate natural monopolies, but the whole point of a natural monopoly is that you can't efficiently break it up**





## ref(m:Monopoly;Monopoly)

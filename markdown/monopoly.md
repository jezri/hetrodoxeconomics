---
output: 'html\_notebook'
title: R Notebook
---

# Main

### Monopoly. So what?

A number of simple economics arguments can be ignoring political
influence against monopolies.

-   Monopolies do not gaunter utility maximization, as they often raise
    prices through price reducing volumes when they have limited ability
    to price discriminant. However price discriminating natural
    monopolies actually may raise output volume.

-   No Darwinian mechanism <!--
    Monopolists may fail to cost minimize becouse although they have a profit motive the market lacks the Darwinistic traits of diversity, selection and inherentence which drive more competitive markets to efficency (@posner1999natural, p574)
    -->
    <!--* Monopolist have several distributional effects, between consumers and between consumers and producers. (@posner1999natural) makes several arguments that having very wealthy monopolist is not an ethical problem and that the gap between the middle and lower classes is of greater concern than the gap between either and the super rich.  **However**
    -->

-   High profits result in political power

<!--
for monopolist give monopolist political power, through lobbying and public campaigns which may lead to further market distortions and regulation in the monopolists interests(@posner199natural, p576).
-->

-   (@posner199natural, p576) Claim that the problem is large firms and
    not monopolists, but the correlation between firm size and market
    concentration seems plausible at least.

-   Higher research budget and incentive

<!--Monopolists may have greater incentive and ability to do R & D, this point is made poorly in (@posner1999natural) and I would prefer a better source.
-->

## Theory of natural monopoly

## Monopoly

### Monopoly

Monopoly:

:   A market where the whole supply is produced by only a single firm
    (@posner1999natural, p548)

#### General behaviour of monopolies

Monopolist are often able to raise prices to capture consumer surplus
and frequently do this by reducing the output volume they supply in
order to drive prices up. The exact strategy they use depends on the
ways in which they can discriminate between various clients and groups
of clients

#### Perfect completion graph

<img src="figure/unnamed-chunk-1-1.png" title="Perfect competition" alt="Perfect competition" style="display: block; margin: auto;" />

#### Non price discriminating monopoly

Here the monopolist must charge all consumers the same price. If
consumers could resell to each other this would be the result.

![](../pics/0eb26b179ef3b424d0b46d0bcd3d1341dad5702a.png)

#### Price discriminating monopoly

![](../pics/73432a5b6c942b356f6dfe76d6c5465c0884ae68.png)

There is significant empirical evident that monopolists do price
discriminate (@posner1999natural, p555). This may reflect in higher
wages to upper management or investment rather than profits.

------------------------------------------------------------------------

Price discrimination allows poorer consumers to purchase goods below
monopoly prices otherwise. In the case of natural monopolies it allows
them to purchase below the cost of a competitive market, as the price of
fixed costs to the firm is payed by wealthier consumers.

Examples are:

-   Pensioner and student discounts

###### Short term vs long term

These models may not be good estimations of the long term behavior of a
monopolist as some reduction in visible profits will help keep profits
defensible from regulators and competitors (@posner1999natural, p559).

##### Effectiveness of regulation

-   Piecemeal breaking up of concentrated markets while large monopolies
    remain in pharmaceutical and agricultural sectors may result in
    distortions in the market? I would need to see strong evidence that
    this is not justified(@posner1999natural, p561).

-   Collective bargaining may be an effective counter to monopoly
    pricing (@posner1999natural, p562). But how does this work when
    consumer groups are far more fragmented, governments are frequently
    corrupt and can be lobbied to overbid.

##### Are monopolies that bad

-   Monopoly profits may encourage innovation and efficiency from
    entrepreneurs(@posner1999natural, p566). As businesses attempt to
    drive down prices to destroy competition.
    -   This fails if the firm becomes able to make defensible monopoly
        profits.
-   (@posner1999natural,p561) claim that quality is likely to remain
    close enough to optimal for consumers.
    -   This follows from a model where consumer demand drops if quality
        drops
    -   However more powerful monopolists will face much less elastic
        demand curves.

------------------------------------------------------------------------

-   (@posner1999natural,p587) claim that the use of price decimation to
    finance a price war is unlikely
    -   They base this on the assumption that high profits following a
        price war will not be defensible.
-   (@posner1999natural,p589) argues that monopolists are unlikely to
    use their surplus to invest in vertical integration as this would
    allow them to use monopoly powers to exploit only themselves.
    However monopolizing an early stage in production or a later market
    in distribution may increase defensibility.

### Natural Monopoly

Natural monopoly:

:   A market where market demand is produced most efficently by a single
    firm. There are several reasons why this might be the case. **A
    natural monopoly does not mean that a market is controlled by a
    monopoly, it may simple be running inefficiently, this will be
    explained later**

#### Reasons for natural monopoly

-   There may be significant economies of scale gained only by producers
    producing close to full market capacity, (find example)
-   Having multiple firms may involve wasted duplication of resources.
    For example imagine building 2 different electrical grids for
    different producers.
-   Networking economies. Everyone wants to have compatible goods. For
    example there is no use using a very good social media platform that
    no one is on, the number of other users determines the quality of
    the service or product.

#### Consequences of natural monopoly

A natural monopoly may frequently result in the market being controlled
by a monopoly.

#### Natural monopolies due to high fixed cost

I will call call consumers willing to pay more for a good high demand
consumers and those willing to pay less for a good low demand consumers
for clarity.

-   A monopoly may need to price discriminant monopoly in order to:
    -   afford to provide the service at all
    -   afford to provide the service to low demand consumers.

------------------------------------------------------------------------

![plot of chunk
unnamed-chunk-4](../pics/97ab07439f5deef2b8bb98842f9078537dfb7c1d.png)

The diagram illustrates that the utilitarian optimal point is where long
run marginal costs meet demand, the producer can only afford to produce
where long run average costs are below demand without price
discrimination.

------------------------------------------------------------------------

The high fixed costs here are in some way equivalent to the concept of a
fixed good in that they are non rivalry, once the fixed cost has been
payed of by some consumers other consumers no longer need to pay for it.
It is exculdable though if the monopolist has the power to price
discriminate they can force some consumers either to pay the higher
price to cover the fixed costs or forgo consumption of the good all
together. However this ability may be limited due to attempts to compete
for the higher paying individuals.

------------------------------------------------------------------------

## Regulating monopolies

(1) believe that natural monopolies only are a small amount of the
    economy and thus regulating them will not fix problems such as
    inequality and poverty. They include only power, trains and water
    supply. However with increasing privatization of government natural
    monopolies, social media platforms rising, is this true any longer.

------------------------------------------------------------------------

When it comes to the case of duplicate infastructure needs we are once
again see that laws that attempt to force the market into competition
infact can cause ineffeciencies(@posner1999natural, p586). However,
possibly this calls for more rather than less government intervention.

------------------------------------------------------------------------

(2) bring up the idea of regulations creating further distortions a lot.

-   firm A has marginal costs $c_a$ and sells at price $p_a$ such that
    $p_a > c_a$
-   firm B has marginal costs $c_b$ and sells at price $p_b$ such that
    $p_b > c_b$
-   Where $c_a > c_b$ and $p_a > p_b$ and $c_a < p_b$
-   If firm A was regulated and costs brought down so $p'_a = c_a$ this
    would cause society to shift consumption towards a away from b in
    spite of b being the less resource intensive good to produce.

------------------------------------------------------------------------

If any neoclassical theoretical analysis has been done to show this may
cause greater inefficiency I would be curious to see it

------------------------------------------------------------------------

-   Would it not be most efficient to allow some legal and ethical
    grounds for price discrimination? Pensioners and students, wealth
    based ext?

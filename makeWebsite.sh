#!/bin/sh
if [ ! -d mdrfOut ]; then mkdir mdrfOut; fi;
if [ ! -d website ]; then mkdir website; fi;
if [ ! -d website/html ]; then mkdir website/html; fi;
if [ ! -d website/pdf ]; then mkdir website/pdf; fi;
cp style/mdrf.css website/html
if [ ! -d pics ]; then mkdir pics; fi;


# Copy and convert docx
if [ -d docx ]; 
  then 
    cd docx
    for d in *.docx; do
      pandoc --extract-media=../pics --atx-headers  $d -s -o ../markdown/${d%.docx}.md
    done;
    cd ../
fi;

if [ -d rmarkdown ];
  then 
    cd rmarkdown
    for r in *.Rmd; do
      echo $r
      echo "library(knitr)" >temp.r
      echo "knit(\"$r\")" >> temp.r
      Rscript temp.r
      pandoc --extract-media=../pics --atx-headers ${r%.Rmd}.md -s -o  ${r%.Rmd}.md
      cp *.md ../markdown
    done;
# Convert library folders
    for D in *; do
        if [ -d "${D}" ]; then
            echo "${D}"   # your processing here
	    if [ ! -d "../markdown/${D}" ]; then
		    mkdir "../markdown/${D}"
            fi
	    for r in $D/*.Rmd; do
      		echo $r
      		echo "library(knitr)" >temp.r
      		echo "knit(\"$r\")" >> temp.r
      		Rscript temp.r
      		pandoc --extract-media=../pics --atx-headers ${r%.Rmd}.md -s -o  ${r%.Rmd}.md
      		cp *.md ../markdown/$D
	   done;
	fi;
    done;
    cd ../
fi
# Make list of links
myString="<li> <a href=\"../index.html\">Home</a></li>"
myStringForIndex="<li> <a href=\"./index.html\">Home</a></li>"
cd markdown
for m in *.md; do
  myString="$myString<li> <a href=\"./${m%.md}.html\"> ${m%.md} </a></li>"
  myStringForIndex="$myStringForIndex<li> <a href=\"./html/${m%.md}.html\"> ${m%.md} </a></li>"
done
cd ../
mdrf "Readme.md"
pandoc -V myCss="./html/mdrf.css" -V pdfFile="./pdf/Readme.pdf" -V otherFiles="$myStringForIndex"  -M title="$m" --template ./myPandocTemp.html --mathjax "ReadmeRef.md" --metadata bibliography="./references.bib"  -s   -o "./website/index.html" 
#--highlight-style=./dracula.theme
pandoc "ReadmePrint.md"  --metadata bibliography="./references.bib"-s  -o "Readme.tex"
pdflatex -interaction=nonstopmode "Readme.tex"
mv "Readme.pdf" "./website/pdf/"
rm *Ref.md
rm *.log
rm *Print.md
rm *.tex
rm *.aux
rm *.out
 

rm "ReadmeRef.md"
cd markdown
for m in *.md; do
  cd ../mdrfOut
  mdrf "../markdown/$m"
  pandoc  -V myCss="./mdrf.css" -V pdfFile="../pdf/${m%.md}.pdf" -V otherFiles="$myString"  -M title="$m" --template ../myPandocTemp.html --mathjax "${m%.md}Ref.md"  --metadata bibliography="../references.bib" -s -o "${m%.md}.html" 
  # --highlight-style=../dracula.theme 
  
  # --highlight-style"../dracula.theme"
  echo "Pandoc conversion successful"
  pandoc "${m%.md}Print.md" --metadata bibliography="../references.bib"  -s -o "${m%.md}.tex"
  pdflatex -interaction=nonstopmode "${m%.md}.tex"
done
  cd ../
cp mdrfOut/*.html website/html
cp mdrfOut/*.pdf website/pdf
cp -r pics website/

# Main

## What is post Keynesian economics?

It is a collection of economic models which focuses on economy where long run demand is below long run supply for significant periods. This differs from the neoclassical consensus which focuses almost exclusively on supply side constraint to output. For post-Keysains crises are a common feature of capitalism not an exception and hence government is frequently necessary in order to maintain full and stable demand level. Another common feature is that uncertainty is used to explain the behavior of economic agents. However due to high uncertainty especially in periods of crises people often keep many assets as in liquid forms which damadges investment in the economy as a whole and further deepens the crises.

In many ways its findings contradict the neoclassical school, it shows benefits of higher wages and more formal employment contracts and the need for government intervention on a macro level. 

Both the reasons and results of this instability and underemployment under capitalism give rise to a number of interesting models. While these are largely compatible it may be more useful to begin by looking at them separately than to try and understand a synthesis of all the different models.

Here I will examine a handful of important ideas from the school to give a sample of the ideas it contains.

## Joan Robinson and Sraffa on Capital(@robinson1953production)


Marx's work showed that owning capital allowed business owners to exploit workers and gain a larger share of the business earnings than they were directly responsible for. In response to this neoclassical economists developed a new model. In this model if there were no unions or other collective bargaining then the income of workers and business owners was solely determined by their contribution to firms profits. 

In the Solow neoclassical theory of growth the rate of profit was determined by the relative amount of labour and capital used in production. Joan Robinson and other post-Keynesian economists believed that this model had errors and was in fact circular. They based this critique on the definition of capital. 

They began with the question of how one aggregated the amount of capital in the economy. One cannot add different types of capital together directly. One cannot simply add computers to tractors. Thus requires measuring the monetary value of capital. However here we arrive at a problem. The value of capital is itself set by the rate of profit. If the price of corn goes up than the value of the tractors used to produce it increases. This means that the Solow models determination of profit rates is circular. The amount of capital determines the rate of profit but the rate of profit determines the amount of capital. 

For post-Keysians this result shows a fundamental floor in neoclassical theories of price determination and in neoclassical theories of distribution. It brings back the question of how wages and profits are set through institutions and policy and historical processors. A question we will look into in more detail in the next section.









## Kaleckis  model of wage led growth (@stockhammer2011wage)

This is an alternative to Keynes model of insufficient demand and focuses more on class. In this model demand can either be wage driven or profit driven within an economy with these different regimes giving very different results for the overall economy. 

The model begins by examining how earning are distributed between workers and business owners. The proportion the business owners get is called the profit share and the proportion the workers get is called the wage share. In a neo-liberal model these are set. Competitive  forces drive down profits and wages until both equal their relative factor productivity. However, in less competitive markets this no longer holds. Instead the wage share and profit share are determined by institutions and policy. Institutions like unions increase the wage share while policy to increase the flexibility of labour markets decrease the wage share. 

This model looks at two important factors in aggregate demand consumption and investment. A rise in the wage share increases consumption but can drive down profits a rise in profit share decreases consumption but can increase investment. This brings us to the central question of whether demand is wage or profit led.

If demand is wage led than increasing the wage share will increase consumption by more than it will reduce investment and so increase aggregate demand. If an economy is profit led the opposite will occur. 

Often smaller open economies tend to be profit driven as decreasing labour costs boosts productivity and increases exports. However larger economies made up of the sum of these smaller economic regions tend to be wage led as increased consumption in one region creates increased demand in another. Further economies tend to be more profit led in the short run and more wage led in the long run.

This implies that neo-liberal growth policies aimed at increasing the profit share may result in slow growth in the long run. However growth appears to be supported in the short run by debt driven growth and speculative bubbles. But wage led demand suggests a possible sustainable future growth path. This increase in wage share must be coordinated between countries otherwise it will make some countries uncompetitive, we will return to this idea in the next section.









## Thirwals open economic model(@thirlwall1979balance)

Thirwel was interest in understand why demand driven economies trading with other countries. His model explains the differences in growth rates between different countries. In the neoclassical model countries grow because of increase in labour, capital or productivity. However the neoclassical model does not explain why these increase. Thirwell explains that these increases are driven by demand for domestic goods. This increases employment and investment leading to increases in the factors of production. Further as demand increases resources can often be transferred to more efficient and profitable uses increasing productivity. 

However in the long run countries cannot afford to import more than they export. Unfortunately the demand in a country is depend on domestic income and  increasing local income increases the amount people import. So the demand in an economy is limited by its international trade flows. If a country increases imports a lot when income is raised it can only have low demand and grows very slowly. If a country increases imports only a small amount as income is raised it can increase demand to near full employment and this encourages investment, increases in productivity and growth.

This implies that the free trade supported by neoclassical economics may prevent countries developing. It is important for countries which need economic growth to be able to limit imports without depressing domestic demand. However if every country limits imports this will damage any countries ability to grow because they can no longer really on exports to bolster demand. So countries must coordinate to all create demand in a way which does not some countries with large trade deficits as a result of their economic stimulus. Further these agreements should be specified to allow high growth in underdeveloped and impoverished areas.

## Conclusion

This theories from post-Keysain thinkers show some of the potential the school has as an alternative to neoclassical economics. 
After decades of stagnate wages and low growth it shows the potential for more equitable and stable regulated capitalist economies. 
There are also numerous attempts at integrating post-Keynesian economics with other heterodox schools, for example ecological Keysainism  models of green new deals. However the theory remains far outside the mainstream, perhaps because of its political implications both for the role of the state and for its support for increasing wages. 

There are many more theories and models to the paradigm and a much larger body of empirical and theoretical debates between post-Keynesian and neoclassical economists. There are also several good textbooks linking all these different writers. Unfortunately however, even introductory texts to the school remain relatively advanced possibly because they stem from sophisticated critiques of the neoclassical school. In general a lot of work needs to be done to increase public understanding of the ideas beyond simply the importance of government stimulus.















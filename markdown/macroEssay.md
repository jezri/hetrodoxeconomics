---
output:
  html_document: default
  pdf_document: default
  word_document: default
---

# Main

<!--

The deadline is Thursday 30 April 2020, 4 pm.

Consider the following statement:

        According to the classical approach, an increased desire to save leads to an increase in investment, via the interest rate mechanism, with the level of income remaining unchanged. In Keynes, however, an increased desire to save leads to a fall in the level of income, and this is generally referred to as the Paradox of Thrift.                            

With the above statement as background, write an essay on the implications of an increased desire to save. Present both the classical view and the Keynesian one.
    [70 marks]

Illustrate the above effect of an increased desire to save by means of an IS-LM diagram. Your answer must consist of both a diagram and an essay. Your diagram must indicate what curve shifts, and your essay must explain what is happening in the diagram.         [20 marks]

Your assignment must contain the list of references you used in this essay. In your text you should refer to the references you used in writing your essay.    
    [10 marks]


References to be used

As a minimum, consult the readings by Hagen, Perlman and Johnson in the course outline. 

Hint 1    In discussing the classical approach you must make use of the type of diagram employed by Hagen. Your discussion must include an analysis of a shift in the savings curve in Hagen’s diagram.

Hint 2    In discussing Keynes’s approach you must make use of the type of diagram employed by Johnson in his Figure 2. Your discussion must include an analysis of a shift in the savings curve in Johnson’s diagram.

Hint 3    Read the first two pages of chapter 16 of Keynes’s General Theory.

Word length for essay: 3 000 words.

-->

## Introduction

Classical and Keynesian models of the macroeconomy predict starkly
contrasting outcomes for the economy if savings in the economy increase.
For the classical model employment and therefor output in the short run
is solely determined by wage rates. Savings does not affect short term
production. Instead in the classical model, all money that is not spent
on consumption is transferred to investment. Here an increased saving
rate reflects a simple trade-off between current consumption and
investment. This increased investment may even speed up economic growth.
For Keynes however, increased savings may not be matched by increased
investment. Instead, the total expenditure in the economy will be
reduced, this will, in turn, will reduce production and employment,
(which may not be restored by drooping wages).

## The foundation of both models: circular flow of income and resources

Both models are based on tracing the flows and income and resources
moving around the economy, and under which conditions the amount of
resources being bought is enough to ensure full employment. Where full
employment still entails some small amount of unemployment for workers
seeking jobs.

In this model, households consume goods and services produced by firms.
Firms pay out all their revenue as production costs to households. Rent
goes to landlords and owners of capital equipment, wages go to workers,
and profits go to business owners(@morgan1980monetarists, p 5). This
model is complicated by additional inflows and outflows. While inflows
and outflows from government spending, taxation, and foreign markets are
important they complicate both models and are not necessary to
understand the different roles that savings play in the two models.
Thus, we will only look at the inflows and outflows from the financial
market. That is savings and investment. Here expenditure is the sum of
consumption and investment.

Investment must be defined precisely in this model. It refers to net
investment in the economy. The sale of old assets is a form of
disinvestment so trades in assets cancel out and only new investment is
counted(@keynes2018general, Book 1, Chapter 7, Part II). However, as
oppose to Keynes's use, I will not use investment to refer to
expenditure or earnings gained from depleting or building inventories.
Thus, while building up inventories would entail firms to spend more on
production costs than their revenue and thus would force them to borrow
to make up production costs this is not considered investment.

In both the Classical and Keynesian models shifting interest rates
affect savings and investment. In these models, a simple interest rate
is used to represent the whole collection of different interest rates
offered to different borrows and lenders in the real
economy(@morgan1980monetarists, p 9). In the classical model, this
interest rate always acts to bring equate savings and investment.
Contrastingly in the Keynesian model output must be varied to equate
saving and investment.

In the classical model, full employment can be achieved for any savings
rate, if wages are dropped low enough. However, in the Keynesian model
either wages cannot be dropped to return full
employment(@keynes2018general, Book 1, Chapter 2, part II) or such a
drop happens only slowly and painfully and can result in long periods of
under full employment. Thus, in Keynsain models increases in savings can
reduce output and employment in a way that cannot be quickly restored by
a drop in wages.

## Savings in the classical model

The classical model is a pre-Keynesian model where the economy reaches
constant stable full employment without government intervention. Here
savings rates are irrelevant to the amount of expenditure in the
economy. This view was widely held by economists before Keynes, though
with many exceptions such as Malthus and Marx(@O_Leary_1942). However,
classical economists did not use a formal detailed model of the whole
economy to come to this conclusion. Instead, they regarded it as a
simple conclusion which needed little justification. Thus, understanding
the views of the classical economists requires reconstructing and
synthesizing a model from their assumptions rather than simply examining
models present in their work(@hagen1966classical, p 9).

The classical macroeconomic model relies on two important assumptions.

1.  Says law: The production of goods increases both production costs
    and expenditure equally(@morgan1980monetarists, p 4).
2.  Labor markets reach equilibrium only when wages are equal to the
    marginal product of labor.

Say's law shows that an economy will have sufficient demand for any
possible level of production. Then this is constrained by the labor
market, where wages directly determine the level of employment. Thus,
full employment in this model can always be achieved by dropping wages
low enough. So, in this model, there is no involuntary unemployment as
workers could always gain employment by reducing their wages.

### Say's law

The classical derivation of Say's law was done for a barter economy.
Classical economists did not think that money significantly altered
economic analysis(@morgan1980monetarists, p 5). To compare the classical
view to Keynes however, and even to show how it applies to real-world
economies it is necessary to adapt it to a model of a monied economy in
which aggregate demand would always equal aggregate demand.

This is where savings are incorporated into the classical model. Here
total expenditure is sufficient so the value of goods produced in a
period is the same as the value of goods consumed. On average
inventories are neither built up nor depleted but remain at the level
desired by firms(@morgan1980monetarists, p 7).

This model relies on two propositions.

1.  Income from production is equal to the costs of production.
2.  All income is spent on goods and services.

To show the first proposition we simply note that all the revenue earned
by a firm in the production or sale of a set of goods and services is
divided up into income of various people. .However, wages and rents are
only paid at regular intervals, and profits are only made after goods or
services have been sold(@morgan1980monetarists, p 6). Therefore the
amount of income received at any point in time is linked with some past
output, not with present output. If the capacity of the economy
increases, say because of an increase in the size of the workforce than
this lag between output and income means that there will be too little
income (paid from previous periods) to offered the expanded current
output. This is a minor flaw with Say's law when applied to expanding
economies.

The second proposition is that all income made in a period is spent on
goods produced in that period. This is derived from a model of the money
market that always clears. The amount of savings can be considered as
the supply of loanable funds and increases as interest rises,
presumably, people will be more willing to save as interest rates
increase. The amount of investment can be considered as the demand for
loanable funds it decreases as interest rates rise. As with standard
classical markets, this will result in an equilibrium market price, the
interest rate where markets clear and there is no excess supply or
demand. This is demonstrated in the diagram below.

![plot of chunk
unnamed-chunk-1](../pics/186e99c5f5ccf3bb691fe838c5abcacc491ddb8e.png)

Like all classical markets, this one may be in temporary disequilibrium
while interest rates adjust. This could cause a temporary imbalance
between investment and savings. However, classical theorists did not
believe this was significant enough to produce prolonged deviations from
Say's law(@morgan1980monetarists, p 11).

### Labour markets in the classical model

The demand for labor in this model is derived from a production
function. Total output is a function of the amount of labor employed.
However, there are decreasing returns to labor. That is additional units
of labor increase output by less and less. Employers will hire more
workers while the real wage paid to workers is below the value of the
additional output they produce. Once the value of additional output
produced by the last workers hired is equal to the real wage rates they
will stop hiring new workers as hiring additional workers would increase
profits by less than the cost of the additional workers' wages. Thus,
the level of employment is uniquely determined by the real wage rate and
labor productivity(@hagen1966classical, p 9-13). The ability of flexible
wages to quickly restore the economy to full employment in the classical
model shows that savings have a negligible impact on employment levels.

### The implications of savings in the classical model

Now we have outlined the classical model we are ready to see the
predicted effects in the model of this increased saving. People will
spend less of their money on consumption and save more of it. Rather
than sit ideal people will want to put savings into interest-bearing
investments. This will increase the supply of loanable funds. Before
interest rates adjust this may result in more savings than investment
which will result in a temporary drop in aggregate demand however in the
classical model this will have no lasting effect. The rise in loanable
funds will result in a drop in interest rates in order to loan out the
funds to investors in the real economy offering increasingly lower
returns. The result is an increase in investment and a reduction in
consumption. In the long run, this may even result in faster growth due
to the increased investment. In the short run, it will not affect total
output which will remain at its maximum or total levels of employment
which will remain full.

![plot of chunk
unnamed-chunk-2](../pics/3be68cb4d5f69a5d2a732b24f96bdd88ff1658d1.png)

In the classical model, there is a direct trade-off between consumption
and investment as the economy is operating at full employment. An
increase in savings will lower interest rates and increase investment.
This has implications for how income should be distributed in an
economy. As wealthier people tend to save more it implies that for high
investment and consequent growth income may need to be distributed more
regressively to ensure sufficient savings.

## Savings in Keynesian models

Keynes gave an alternative model of the economy where expenditure often
was insufficient to maintain full employment. This model was developed
in response to the prolonged unemployment caused by the great depression
which classical economists were unable to explain with their model. A
common, if debated interpretation of Keynes's work is the model
developed by Hicks. This model is based on the interactions between, the
goods market, the money market, and the labor market. The goods and
money market combined to form the IS-LM relation. However, the lasting
effects of increased savings in the Keynesian model depend on the
interactions of the IS-LM relation with labor markets.

### ISLM model

The ISLM model is one that models the interaction between the goods
market and the money market. Both markets are only in stable
equilibriums at certain combinations of interest rates and output in the
economy. For the goods market, this is represented by the IS curve and
for the money market by the LM curve. Thus both markets are only
simultaneously in equilibrium for a combination of interest rates and
output which clears both markets. Given certain assumptions about the
shapes of the IS and LM curve, this combination is unique. In the market
for loanable funds still always clears(@keynes2018general, Chapter 7),
however, this clearing often implies businesses take loans to build up
unwanted inventories or do not invest sufficiently to maintain the
inventories they planned for. Thus, in this model, though savings and
credit taken out are always equal for all levels of output, the
difference between loans and planned investment causes businesses to
change their level of output.

### IS The goods market

In an economy, the expenditure on goods and services comes from
household spending a portion of their income on consumption and from
investment. If expenditure is not equal to production costs this will
cause firms to adjust production bringing the goods market back to
equilibrium output. This output is below maximum potential output and
further higher savings rates are the lower this equilibrium output will
be. However, due to investment output varies inversely with the interest
rate is.

Expenditure and production costs converge because of changing output.
Expenditure includes only a portion of household income from production,
so a cut in output reduces production costs by more than it reduces
expenditure. Similarly, a rise in production increases production costs
by more than it reduces demand. Thus if expenditure exceeds production
costs, inventories will be depleted and production will expand, this
will cause demand to expand as well but by less than the expansion in
production. So, after this, the difference between demand and supply
will be lower. This will continue until demand is equal to supply. The
opposite process will occur if supply exceeds demand.

However, for any given interest rate this equilibrium will be different.
The amount of investment depends on the interest rate. Thus, for any
given interest rate there is a different amount of investment, thus
creates a different amount of expenditure. Further, investment will
increase demand by more than the value of the investment. This is
because some of the income generated in production of capital bought
with the investment will be spent on consumption by the household owning
factors used to produce the capital. Consequently higher interest
results in lower investment and lower equilibrium output. This
relationship between investment and equilibrium output in the goods
market is known as the IS or investment-savings curve.

Another way of viewing this equilibrium is a point where savings is
equal to investment. Here demand exceeds supply if more money is saved
than invested and demand exceed production if more money is invested
than saved(morgan1980monetarists, p 24).

### LM The money market

Another relationship between output and demand is determined in the
money market. Here we arrive at an equilibrium of demand for money and
supply of money. Here the equilibrium interest rate is determined not by
the rate at which all savings will be matched with profitable
investment, but by the demand and supply of liquid and illiquid assets
primly money and bonds(@keynes2018general, Chapter 16 part I).

In this model the amount supply of money is constant. This is some
multiple of the amount of actual currency in the country created by
fractional reserve banking the money is held. However, we are not
interested in the nominal quantity of money but instead on the value of
money present in the economy. Thus the supply of money is given by the
amount of money divided by the value of money(price levels).

Demand for money, however, depends on multiple factors.

People keep a certain portion of their income as money rather than
investment in order to spend and this depends on their income levels.
The greater their income levels the greater their expected expenses.
However, in addition to this, and contradicting the classical model,
people save a certain portion of their wealth as money rather than
investing it in interest-bearing investment. This is because some people
are cautious at any given time that the value of investments in the
economy is likely to decline relative to the value of money as that this
deprecation would outweigh the value of interest on the
investment(@keynes2018, Chapter.

<!--
Although liquidity preference makes sense the actual mechanism must be questioned. People don't literally hold cash as savings even in depressions. Is there a difference between holding deposits and investing, in this case how to deposit earn interest? Follow up on this but not in this
-->

If the quantity of money demanded exceeds the quantity of money supplied
interest rates will rise, this will make investment more attractive and
decrease the quantity demanded until demand and supply are equal. The
opposite will happen if supply exceeds demand. For each level of income,
the interest rate needed to maintain this equilibrium will be different.
The higher income is the more money will be demanded transactional
purposes, thus a higher interest rate is needed to maintain equilibrium.
This positive relationship between output and interest rates which bring
equilibrium in the money markets is called the liquidity money or LM
curve.

### Combination of the two curves and the labor market

The combination of the two curves results in a unique stable equilibrium
in both markets. This combination determines the total amount of
production in the economy. This, in turn, determines the amount of
employment in the economy. In some models, this implies that wages have
no impact on the level of employment(@morgan1980monetarists, p33-p36).
However, in other models, there is a mechanism for a reduction in wages
to return the economy to full employment.

This is present in the common aggregate demand, aggregate supply model
which constrains ISLM to a short term determination of market output.
Here we begin by observing the link between the LM curve and price
levels. A reduction in price levels increases the money supply and so
shifts the LM curve to the right. If investment is can be increased
sufficiently by a fall in interest return the economy to full
employment(@johnson1962money, p 80). This creates the possibility for an
ISLM equilibrium with full employment if price levels can be reduced
sufficiently. Nominal wages are linked to the price level therefore if
wages decrease this decreases price levels. However, weather decreasing
wages will reduce price levels depend on the degree of competition in
the goods market. However, this model has been criticized because ISLM
equilibrium may not have such a clear inverse relationship with price
levels. Fractional reserve banking may reduce normal constraints on
money supply, further investment may be spurred by increasing prices or
inflation as this reduces the real interest rate(@bhaskara2007nature).

### Savings reduce consumption (500 words)

If we look at hicks ISLM model an increase in savings will lower
consumption at any given rate of interest in the goods market. This will
reduce consumption resulting in a downward shift of the IS curve while
the money market remains constant.

![plot of chunk
unnamed-chunk-3](../pics/9e3127beb69cb29d762d1699caae054af2078570.png)

This results in a lower equilibrium output at a lower interest rate. The
net result may increase or decrease investment depending on how
sensitive investment is to changes in interest vs how sensitive it is to
changes in total expenditure. The result of this Keynesian model is the
paradox of thrift. That is that an increased savings rate may reduce
income so much that it could reduce the actual value of savings.

While this change will cause a temporary decrease in employment the
permanent effects on output depends on whether or not shifts in the
labor market, particularly wages and resultant price levels can adjust
to compensate for the reduction in expenditure by increasing the real
money supply, shifting the LM curve outwards enough, in the long run, to
compensate for the leftward shift in the IS curve.

## Conclusion (500 words)

In the classical model, an increased desire to save is an economic
benefit. Savings increase investment and leave output at constant
levels. Further, this increase in investment at the cost of present
consumption may lead to increased growth in the future. For Keynes,
however, an increased desire to save may have disastrous effects. As
people begin to save a greater proportion of their income they reduce
their consumption spending, however, in Keynes's model this is only
somewhat compensated by an increase in investment due to lower interest
rates. This results in a loss of output and possibly even a reduction in
total savings levels.

# Main

## Neoclassical theory of perfect competition

We have just examined the dependence of neoclassical theory on perfect competition. Now we will look at the critiques of perfect competition. 
We must begin by examining the model of perfect competition itself. The neoclassical model of perfect competition is not unique or unified. Different neoclassical economists have reformulated it responding to many critiques. Thus in critiquing perfect competition I may have missed some more advanced mutations of the model.

Perfect competition:

: A model of markets based on  restrictive assumptions. Market behaving according to this model are productively and allocativly efficient in the interests of the monied classes.

Assumptions include:

 #. Many independent buyers and sellers
 #. Identical products
 #. Well informed consumers
 #. No barriers to entry or exit

The neoclassical theory of perfect competition is an intertwined theory of the firm and theory of the market. The firms behavior and the type of firm that exists is determined by the competitive nature of the market. Further the result of many such firms is a competitive market. So the model must show two things in order to be consistent.
 
 #.  In a competitive market all firms behave competitively. 
 #.  A market formed out of firms behaving like this is competitive.

In simple neoclassical models the supply curve for the industry is simply determined by taking the sum of all the individual firms average costs curves. The demand curve is similarly determined by summing individual demand. An important point in neoclassical economics is that prices work as a tool for consumers to signal to producers what they value. However this would require a sloping supply curve. Later we will see how this relies on assumptions which make it impossible to analyze industries in isolation(@sraffa1925).

We show firms in this market behave competitively by assuming there are identical products and perfect information. This  means that no firm can sell units above the market price and that there is no incentive to sell below market price as this would not increase quantity sold. So all firms follow the standard formula for profit maximization producing at the maximum quantity where marginal revenue is equal to marginal demand.

Next we show that this results in a stable competitive industry. Firms must not be able to produce larger quantities competitively or they would gain market power. So the relation between quantity and firms costs must form a **U** shape sloping up after a certain quantity. This relies on several assumptions about internal economies of scale which we will critique later. Thus in the long run because there are no barriers to entry new firms can always enter the industry if it starts to make high profits this drives profits back down to normal levels. Finally firms which cannot make break even exit the market in the long run. This ensures productive efficiency because inefficient producers eventually leave the market.

In this model all firms have the same costs in equilibrium because the value or rental costs of factors of production are modeled as varying the output of the industry. So, for example the more wine is produced the more valuable good vineyards are. Consequently owner of better wine lands are not modeled as having higher profits if wine prices rise but rather as having normal profits and higher costs as the value of their input (the vineyards rose). Here we see that the costs of an individual firm are dependent on the output of the whole industry. The opposite effect can also take place, industry wide economies of scale can mean firms costs drop as the total production of the industry rises. This makes it impossible to find the industry supply curve by simply summing all individual firms supply curves.  Sraffa suggests a more complete model to find industry supply where firms costs are a function of industry production. However this model is uncommon, and many of the critiques of perfect competition including Sraffa's can be made with only informal inclusion of industry wide economies and diseconomies of scale. So we will use the simple model above for the rest of the essay.


## Old stuff

In response 



Using definitions of economic costs firms make only normal profits in the long run. Finally expansions or contractions 





Examining the production function individual firms operating under perfect competition. The firm will produce the maximum quantity where marginal costs equal marginal revenue.

The marginal revenue for the firm is given by the price they can charge. The assumptions of perfect information means that they cannot sell at a price above the market price. 

Given some value of $Q_{total}$ for firms where marginal revenue is below marginal cost for all level of production $Q_i = 0$.

for firms where marginal revenue equals marginal cost for some range of quantities $Q_i$ is the maximum of those quantities; because of our accounting for cost no firm can exist where marginal revenue is above marginal costs.

This gives our unique $Q_i$ as a function of $Q_{total}$

Although it is necessary for perfect competition that firms have **U** shaped cost curves with respect the quantity they produce in reality this is unlikely and relies on the existence of substantial internal diseconomies of scale.
Firms 
That is they are price takers who produce at a point of productive and allocative efficiency, further the firm must experience diseconomies of scale at far below the  )


### A market formed out of a number of firms acting this way is competitive. 

Firms will enter and exist in an organized pattern leading to a sloping industry supply which gives a unique equilibrium and drives all profits in the industry down to normal levels.

#### Problems with aggregation.


The cost of each firm is the cost that would result from full remuneration of market value. So if a firm appeared to be making a profit this would be due to some better factor of production which would actually be worth more and so raise costs. I.E all profits are accounted for as factor incomes instead. As $Q_{total}$ increases production rises these factors become more valuable so firms with better factors have the same costs as those with worse equipment. In addition firms may benefit from external economies of scale as $Q_{total}$ increases. Further the costs of production for a firm vary with the quantity  the firm producers $Q_i$ in order to maintain conditions for perfect competition this must form a U shape so as not to incentive to large a too small a firm size. Further marginal and average costs must be equal at the bottom of the U.
However, the competitiveness of the market is defined by the firms which comprise it.

The most common model begins with an upward sloping total demand in an industry and a downward sloping total supply for the industry. This gives a very important result in neoclassical economics, namely that price is determined by both supply and demand which makes it useful as an allocating mechanism. From this curve the industry demand curve and the  for an individual firm is derived. 

A firms output is given as a function of total industry output and total industry output is given by summing individual firms output. This system appears to be over constrained or at least to give a unique solution for $Q_{total}$

$$Q_{total} =\sum Q_i = \sum f_i(Q_{total})$$

### Production function for individual firms operating under perfect competition

The following model is taken from (@sraffa). Although it is part of a critique of the model I find it more clearly articulated then other explanations.




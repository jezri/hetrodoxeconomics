---
title: "Perfect competition the center of neoclassical economics"
---

# Imports

import package: (hetrodoxEconomics) document: (firms) as: (firm)
import package: (hetrodoxEconomics) document: (summaries/sraffaSummary) as: (sraffa)
import package: (hetrodoxEconomics) document: (neoclassicalPerfectCompeition) as: (neoPC)




# Main

<!-- Try make info graphics involving dairy industry as this example leads into systems of production -->

<!--
Question:

:	The **perfect competition** model is riddled with many shortcomings and incoherencies in terms of its analytical tools. However, the general abandonment of the assumption of perfect competition would have very destructive consequences for **neoclassical microeconomic theory.**

-->
<!--2500 words-->

## Introduction 

<!-- make the jewel metephor the whole thing -->


Perfect competition is the only efficient market model in neoclassical economics. This makes perfect competition foundational and central to neoclassical micro economics. It is foundational because the assumptions of other neoclassical models are based on competitive surrounding markets. Neoclassical theory explains how uncompetitive markets interact with competitive ones giving predictable and stable if suboptimal results. An example is, a monopoly buying raw materials from competitive markets and selling to a large number of independent consumers. But neoclassical analysis of the interactions of non-competitive markets with each other and with other non-market institutions result in unstable and unpredictable behavior. For example, markets with few buyers and sellers,  or the effect of market concentration on political power of firms. Thus, neoclassical economics explains very little in a economy without widespread perfect competition.

Other theories explain these interactions better by focusing on more socially and historically based analysis.  

Perfect competition is central because political motivations exert a force pushing other models in neoclassical economics closer to perfect competition. Assumptions are added to other models to make them closer to perfect compeition.

However the theory of perfect competition is one with numerous critics from other economic schools. And many models of it have shown to be contradictory. Thus, criticisms of perfect competition undermines the theory of neoclassical economics as a whole.

<!--
In this essay I will look at what neoclassical economics is and why it is so centered on perfect competition. Then I will examine the neoclassical model of perfect competition and some of its critiques.  Finally I will examine some alternative models of competition from other schools of economic thought.

-->

## The politics of neoclassical economics and perfect competition   

Neoclassical theory must be accepted or refuted on its ability to describe and predict real world economies.  It must be largely internally coherent and contain a large set of tools to describe many real world scenarios with relative accuracy. However, it can be analyzed as a political tool. In its choice of assumptions, in its interpretation of results and in the forms in which it is simplified when explained to non-academic audiences there is a constant support for neo-liberal politics. This is vital to understanding the central importance of perfect competition in neoclassical theory.

Perfect competition models give a number of outcomes which can be moralized into social goods.

* Perfect competition is a market model where producers serve consumers preferences. This model is still elitist, consumers with more money count more, but it is less transparent or politically motivating than a model where large corporations control the economy.
* Perfect competition maximizes social welfare in certain models of utility.
* It ensures efficient production. Or at least production at lowest private cost.

So policies associated with perfect competition can be politically justified. By extension if most markets operate near perfect competition without government intervention then limiting government intervention is politically justified. This explains the emphasis in neoclassical economics of overextending competitive models.

## Neoclassical theory what is it anyway?

Neoclassical microeconomics is the dominant method of economic analysis. It focuses on markets rather than other structures of production, allocation and distribution. In this theory trade, not production is the source of value. This allows it to act as a strong critique of Marx's work which developed from the classical labour theory of value. This reflects neoclassical economics role in defending capitalist systems of production.

Neoclassical economics abstract form is a flexible calculus with a thousand degrees of freedom.  This can extend to explain a host of market complications and a horde of consumers and producers. But it can only do it in the form of a vast unsolvable set  of maximization problems. So, neoclassical theory collapses these degrees of freedom, assumption by assumption. This can reduce a vector field of consumers in product space to a simple market model. Often this process magnifies competitive elements of these markets and removes distorting elements. Like a diamond cutter sees the gem in the uncut stone, the classical economist sees perfect competition hidden in models of failed markets. This is done by  adding proxies of the various assumptions for perfect competition to other market models.
Some examples include:

* Adding potential new competitors to monopolies and oligopolies to increase competitive behavior. @Baileyt2012DeregulationAT
* Introducing market of product reviewers and journalists to remove imperfect information. (Though information economics has shown that such information markets do not bring enough information for neoclassical models to become competitive and stable@10.1257/00028280260136363 )
* Introducing entire missing markets into models to deal with externalities. @10.2307/2526916

Neoclassical microeconomics has two main approaches. The Walrasian approach analyzes the whole economy to show general equilibrium. This results depend on markets operating under perfect competition. The Marshellian approach focuses on explaining individual markets with unique stable equilibrium. It contains market models ranging from perfect competition to monopoly. Further, it can model several types of markets failure such as externalities.  These other models would remain if perfect competition were removed.

However, they have their own shortcomings. They assume relative isolation from other market and non-market forces, including these forces only as constant exogenous factors. Further Marshellian models of uncompetitive markets do not reflect realities of firms fighting to maintain long term defensible profits. Instead their analysis focuses on markets which are uncompetitive only in the short term. They model firms as making high profits which will soon attract competitors. Alternately they model uncompetitive markets as contestable markets where firms are still forced to behave relatively competitively to deter new firms entering the market.

### Other schools related to neoclassical economics

Other economic schools of thought augment neoclassical economics giving a more robust if less politically useful model of the economy. 
Schools of thought such as new institutional economics, game theory, information economics and behavioural economics all have relatively compatible methodologies to neoclassical economics relying on individual and ahistorical analysis. Many of these theories are less openly dependent on perfect competition. Further they explain non-markets. This is important if perfect competition is impossible, because then non-markets may be more efficient in many industries.  So, would adding additional features from these critiques build a version of neoclassical economics which acts as a useful explanation of the economy without perfect competition?

I cannot begin to fully analyze the interaction of these schools with neoclassical economics. But, there are several reasons I believe they cannot save neoclassical economics after perfect competition has been removed.

Even these schools explanations of non-market institutions are reliant on neoclassical competitive models in a number of ways.

 #. They model non-markets  as pseudo competitive markets. For example, North model states behaving competitively were one political regime has to compete with other potential regimes in order to continue to rule a country. They do this to avoid revolutions, coups or electoral losses, so the state is competing with other potential states(@fine2000economics, p6). 
 #. They explain non-markets simply by the absence of competitive markets. For example, using undeveloped labour markets to explain slavery. Comparing slavery to a hypothetical competitive labour market which could never have existed is not an real explanation. That would require analyzing historical and social reasons for slavery. 
 #. They model institutions poorly to support markets in other areas. For example, North models states as existing to enforce property rights (@milonakis2007douglass).

Thus, non-market models compatible with neoclassical models are also damaged by critiques of perfect competition.


Other neoclassical adjacent schools such as game theory and information economics build explicitly uncompetitive models. As edge cases they make neoclassical economics appear more nuanced and robust than it truly is. However their general results are far from those of standard neoclassical theory and may themselves justify abandoning traditional neoclassical economics. However their acceptance in mainstream economics contains criticism of neoclassical models to avenues that are politically safer to capitalism. These critiques of neoclassical models they offer contain no class based analysis, are too complicated to be widely understood and have few clear policy implications. Consequently these schools cannot save neoclassical economics from its reliance on a flawed model of prefect competition.



## ref(neoPC: Neoclassical theory of perfect competition)

<!--

Begin this model without rents or prices that depend on the quantity produced by the industry. Only introduce this element in the sraffa critque.

-->

## Critics of the neoclassical theory of perfect completion


### The critique of internal diseconomies of scale.

As discussed above competitive firms must have **U** shaped costs curves otherwise a single firm could expand production to gain a significant market share and so reduce competition. Many neoclassical economists give various different reasons to justify the existence of internal diseconomies of scale in excess of those faced by the industry as a whole. However few of these reasons seem  few of these explanations seem probable limitations to the efficient expansion of real world firm. Rising costs due to fixed capital equipment or any other limited resource compared to other resources is one common explanation for diseconomies of scale. However this must apply to the industry as a whole and not an individual firm. It does not explain why new entrant will produce more efficiently than existing firms expanding production. Another explanation is increasing managerial inefficiency. However, it still seems possible for larger firms to compete, through decentralized management structures(@aslanbeigui1997scissors).


### ref(sraffa:Sraffa critique)







<!--

### Information theory critique


The information theory critique focuses on imperfect informations effects in many markets resulting prices differing significantly from prices under perfect competition, even in markets with many buyers and seller(@stiglitz2000contributions). There is also a claim that price differentials will not be solved by arbitrage from more informed market agents(@stiglitz2000contributions). Even in a model with a clear industry price it may pay a single supplier to charge a different one in the context of imperfect information.
Presumably to attract better workers.(@stiglitz2000contributions, p17)
Although neoclassical analysis has attempted to build around this by building models of consumers who place a marginal cost and marginal value on information. This explain only agents confined rational behavior and not the market equilibrium which will be derived from them.

However increasing knowledge for selection criteria to stop welfare behavior can result in welfare lose as oppose to grouping all claims, This would suggest that methods to make markets closer to perfect competition may in fact result in welfare loss(@stiglitz2000contributions). Which undermines ideological claims about perfect competition maximizing welfare


-->
<!--

## What are alternative theories

<!--Just because the neoclassical theory of competition does not hold does not mean that firms do not often behave in competitive ways.-->
<!--
In heterodox models competition as a dynamic process to increase market power and maximize long run profits. Here firms compete in order to gain a market position from which they can make defensible profits. These profits are defensible through:

 #. threatening of price wars. (The price wars involved in these competition tend to push wages down.)
 #. gaining brand loyalty by advertising
 #. vertical integrating to limit competitors access to resources and markets.


Ben Fine discuss an analysis based on systems of production. These are vertically integrated firms or sets of firms acting in coordination, Which Ben Fine suggests may have sufficient power to yield their analysis as separate markets misleading and less useful. His research showed the systems in food production rather than responding to consumer demand rather found new markets for the same goods. For example the milk industry responded to a decline in demand for high fat milk following health concern by redirecting cream to other food producers and increasing marketing from creams and cheese. Similarly the sugar industry facing a lower direct demand from consumers began channeling sugar to producers of packaged goods from soft drinks to biscuits.

Finally there is the possibility that competition can lead firms to behave in ways that are detrimental in the long run. Such as quickly depleting resources in order to remain competitive or not investing sufficiently in future technologies because there is no surplus to invest.

-->

## Conclusion


The question is what must be forgone to restore neoclassical economics if perfect competition is removed. For example, bounded rationality with imperfect information is a type of rationality. But does that make an individual acting in such a way, well described by neoclassical economics.
The theory of perfect competition is an unrealistic one based on strict assumptions necessary for an ideologically and politically motivated conclusion. The theory does not reflect the real world and may not even be internally consistent. However the theory has proved highly adaptable and can still incorporate many critiques, all be it by accepting increasing complexity sometimes in ways which do not reflect the real world (For example by assuming missing markets in some sector auxiliary to the market in question such as information markets.) Nevertheless, several well developed critiques of perfect competition do exist.
So what is neoclassical theory without perfect competition?

A theory of market failure caused by large firms. Although in some ways this theory of monopolistic competition, oligopoly and monopoly are a more accurate representation of the real world, Neoclassical economics still lacks a way to aggregate such markets into a whole economy. Further its primary reliance on a historical static analysis makes it a clumsy tool for detailed economic analysis. Even its independent models often rely on well behaved supply curves for factor inputs and other assumptions that the rest of the economy is behaving close to equilibrium. Consequently the flaws with the model of perfect competition undermines neoclassical economics as a whole ,both as a tool for supporting neo-liberalism and as an academic theory.

<!--
### Is neoclassical micro useful without perfect completion

> "But it would also be a mistake to exaggerate the differences between Becker and Stiglitz, the old and the new, not least because representatives of the latter often present themselves, falsely, as breaking with neoclassical economics by identifying it with perfect competition" (@fine2000economics, p8)

Certainly neoclassical economics can be mutated into something more complex but is this complex version of neoclassical economics still neoclassical economics. It would be mistaken to expect a text book level analysis to hold up to rigorous examination but if neoclassical economics is fully reworked without perfect competition we get an economy with multiple unstable oligopolies and monopolies with unlimited production engaged in strategic fights for market dominance. These challenge a lot of equilibrium analysis and suggests that markets alone a volatile and inefficient. This is far from what most modern day neoclassical analysis use as base assumptions.
-->

---
title: "The ownership and mandate of the South African reserve bank. A democratic formula for economic growth or a tool of an economic elite"
author: "Jezri Krinsky"
---

# Main


<!--

**Question**

Make the case for or against a change in the mandate and ownership of the South African Reserve Bank? 3500 words -->

## Introduction

The mandate of the central bank is to maintain price stability in order to promote sustainable growth in the South African economy. It's emphasis on price stability over employment and growth may, however,  be holding back South Africa's growth and output. The private ownership structure has little to do with the policy chosen by the central bank. Hence, although it is important to examine the ownership of the reserve bank, it is not a major macroeconomic issue for South Africa.

Both inflation and low investor confidence constrain the policy choices of the central bank. Expansionary monetary policy and inflation are considered to negatively affect investor confidence. But, contradictionary monetary policy may not be the safest approach. This is because it may be a cause of inequality, low growth and high unemployment. These could deter investors much more than a moderate increase in inflation. 

Debates over the ownership and mandate of the South African reserve bank happen in a wider context of ongoing disagreement about the role of government and private investment in addressing systemic inequality and poverty in South Africa.  Everyone sees South African economic failures: high unemployment, poverty, low growth, an potentially dangerous debt to GDP ratio, government deficit and poor trade balance. However, people see the causes differently. 

On the right, there is a view that the main issue lies with the state and organized labor. A growing, inefficient and corrupt state is hindering development. Further, unions are keeping wages too high for development and higher employment rates. Finally, there is continual pressure for institutional change (nationalization, land reform and changes to central bank mandate). These are discouraging potential investors. For the right therefore any change to the central bank mandate must be vigorously opposed.

On the left, decades of neo-liberal policies are seen as holding back South Africa's economy. These include increasing economic and financial openness, putting many local businesses under strain and forcing some out of the market. This increases our dependence on volatile international finance markets. Another perceived problem on the left is a lack of expansionary macroeconomic policy to combat unemployment. Central bank ownership and mandate have become central to this debate. Central bank policy in South Africa has historically been a more conservative aspect of South African macroeconomic policy.
Those in favor of changing the mandate argue that:

1. South Africa should be more strategically closed and less vulnerable to the flight of foreign investors.
2. South Africa will  attract more foreign investors by using expansionary monetary policy to ease political, social and economic tensions, than it will deter with moderate inflation.  As these tensions themselves create investment uncertainty.

A change in ownership is symbolic. Nationalization would bring the SARB's ownership in line with many other countries and is more in keeping with democratic principles. However it would have little effect on the behaviour of the institution.  A change in mandate is the more significant proposal, as a shift to more progressive macroeconomic policy could stimulate South Africa's economic growth especially at this time. Here the central bank's control of interest rates can help stimulate demand in the economy during a recession.

### Disaster policies  

The effects of lockdown due to COVID 19 are unprecedented and have forced SARB to adopt numerous highly unorthodox policies in attempts to reduce the damage done to the South African economy. Interest rates have been dropped drastically(@interestCuts) and the SARB has engaged in widespread bond buying(@qe) in order to secure financial markets. These policies are expansionary responses to crises. The long-run effects of such policies remain to be seen. But, this should neither be heralded nor feared as a long term change to the SARBs policy. While the issue of whether this stimulus is too little or too much to account for the current crises remains a separate debate from the long run mandate of the Reserve bank. 


## Definition and role of the reserve bank.

Central banks are important public institutions in many countries, controlling many key macroeconomic tools. They control  the amount of hard currency circulating in a country. Further central banks control the interest rate that they lend to commercial banks at (the repo rate).  Obviously, a country does not have a single interest rate but a whole host of interest rates on different assets. However, the market for all of these financial assets is related. A drop or rise in the interest offered by one asset will have a similar effect on the whole collection of interest rates in a country. The repo rate has a extremely strong impact on all interest rates in a country. 

Beyond controlling repo rates central banks have historically had several other policy tools. They have typically been involved in regulating the commercial banking sector. In particular they dictate the reserve ratio. In addition, they have historically engaged in forgiven exchange(@steiner2017central) markets to influence exchange rates. Additionally they interact with local financial markets, buying and selling bonds, typically government ones. This can also affect interest rates as well as add liquidity to financial markets. This is known as quantitative easing  when it is done on a large scale(@gagnon2016quantitative). Hence the impact of the reserve bank on the economy is complex and extensive.

## Reserve bank ownership 

The SARB is currently privately owned by a collection of shareholders. However, private ownership has little effect on the bank's operation(@rossouw2016private, p1). Recently the ANC has discussed nationalizing the central bank. This has been done successfully by many countries. However, it still may pose risks for investor confidence. Further, nationalization may be the first step in removing central bank independence. We will discuss this later because it is more closely related to the policy of the central bank than its ownership. 


### The private ownership structure of the SARB

The SARB is one of the few privately owned central banks globally. There are large differences in the private ownership structure between these banks(@rossouw2016private, p1). In most instances including South Africa, shareholders are paid dividends and are allowed to vote in general meetings for the bank(@rossouw2016private, p1). The dividends are however low and the majority of any profit goes to government(@bartels2016no)

In some other privately owned central banks the governments still owns the majority of shares and so has a significant vote in these meetings. In contrast, in South Africa, the maximum share the government can hold is limited to $0.5\%$ of shares(@rossouw2016private, p156). So private owners are the dominant voting block, though the share of votes any individual, corporation or bank can hold is limited.
Further, seven of the fifteen members of the board of directors are appointed by shareholders votes(@SARBBOD), although these candidates are vetted by other organizations (@SARBrecentPolitics). Consequently, private shareholders have some limited control over the management of the central bank.

### Minimal policy effects of private ownership

Some research has shown that private ownership does not significantly affect the actions of central banks.
(bartels2016no) find no significant trend towards profit maximization for reserve banks with private shareholders. Further, both (@rossouw2016private, p154) and(@cbFinances, p7) claim that there is no significant effect on monetary policy of private ownership. However, given the wide range of policies of the SARB from monetary policy to regulation of private banks, it seems difficult to definitively test that private shareholders never use their considerable power to influence the central bank for private benefit. 

(@rossouw2018institutional, p7) suggests that private shareholdings increase accountability and transparency of the reserve bank to the general public. However, it is debate whether accountability to share holders translates into accountability to the general public. While no shareholders can own a large enough percentage of shares to control a significant vote this private ownership ensures that wealthier South Africans and corporate interests have a much larger say in the operations of the central bank than the rest of South Africa. In conclusion, although there is no significant evidence of private shareholders influence over central bank policy, it is a potential danger. One which many other countries have seen as important enough to justify nationalization.

### What would nationalizing the SARB mean

Internationally many central banks such as the bank of England were private for-profit banks. Eventually, these grew in scale and became central to the financial well being of the country. Thus many countries saw a need for these banks to shift from profit maximization to more public economic goals(@blancheton2016central). Particularly, following the great depression, many countries such as New Zealand nationalized their reserve banks. This was part of a general strategy for greater government control of the economy to ensure stability and full employment(@rossouw2016private, p152). These nationalizations were done in order to give more government oversight over the actions of the central bank.  Recently the Austria, commercial banks owned significant shares in the central bank. This created a conflict of interest as the central bank was directly responsible for regulating the commercial banks. To eliminate this potential conflict of interest Austria nationalized its central bank(@rossouw2016identifying, p153). However, increasing government oversight  can often be done without changes in ownership.

There have been recent suggestions within the ANC that there may be future attempts at nationalizing the reserve bank, though the party appears divided on the issue(@SARBrecentPolitics). Similarly, views within the party about how this change in ownership may be coordinated with changes in mandates and independence differ widely. However, many economists even ones who favor a shift to more expansionary monetary policy, argue that both nationalization and reducing the central bank's independence may have serious detrimental effects on investment but that policy changes within the current institutional framework would both be relatively safe and also have important effects for growth and employment(@padayachee2019can, 14)

Further, nationalization may be an expense that the state cannot afford. If the shares are bought back at too high a price this transfers money from South African taxpayers to share holds. This is particularly difficult under such constrained government budgets and also may widen inequality as oppose to other ways of spending tax revenue. If shares are bought back at too low price and it could damage investor confidence in other South African institutions.

In conclusion, the private ownership of the central bank although anomalous and in some ways contrary to more democratic aspirations of the country seems too costly to change. Especially as it has little provable benefit.



## Reserve bank mandate 

The mandate of the SARB is to "maintain price stability in the interest of balanced and sustainable economic growth in South Africa"(@SARBMandate). This is done by using monetary policy to keep inflation within targets set by the government. Here, we come to two questions. 

 #. Is price stability the best goal for monetary policy in order to achieve balanced and sustainable growth? 
 #. Is inflation targeting the best approach to maintaining price stability?

The first debate centers around the effectiveness of Keynesian stimulus both in the short and long term. The current mandate is based on a more conservative approach. This approach suggests that expansionary monetary policy will do little to expand output in  South Africa which is constrained by supply-side factors. Further, they emphasize that expansionary policy may increase inflation, posing long run dangers to South Africa. A more Keynesian approach suggests that expansionary monetary policy, although limited may be useful to stimulate the economy. Further, they see less danger of inflation. Finally, their arguments for  more expansionary monetary policy are not made in isolation. In fact, for these policies to be most effective shifts would be required in other areas of South Africa's macroeconomic policy.


### Inflation targeting in South Africa

An inflation targeting mandate involves government setting a band of acceptable levels of inflation which the central bank is then required to try and achieve.  It has several theoretical advantages over other methods for maintaining price stability, such as controlling monetary supply. Inflation targeting is widely understandable and it is easy to see if central bank targets have been met. Thus, it increases central bank accountability. While these may have some benefit, inflation targeting does not seem to cause a large difference in inflation itself(@matter2005does) as many non-inflation targeting countries follow similar policies. It was first introduced informally for the South African Reserve Bank in 1998 and become a formal goal for SARB policy in 2002(@doi:10.1080/02692170701880775, p 244). In short, it is a well established orthodox approach to monetary policy(@matter2005does).

This policy is aimed at creating an appealing investment prospect for foreign investors as a strategy to create economic growth. However, the capacity of developing countries to attract significant foreign direct investment may be limited. In particular, the role of neoliberal policies in attracting such investment may be overestimated. Further, the role of such investment in stable development may be overestimated both from portfolio investment and foreign direct investment. This means that a central bank mandate aimed at securing foreign investment may be misled(@epstein2002employment, Section 2). In addition, there are mixed findings as to whether foriegn direct investment crowds in or crowds out domestic investment. Moderate inflation rates are may not be a significant cause of FDI(@epstein2002employment, p9)

The economic theory on which the inflation targeting is based is built on certain key assumptions and results(@setterfield2006inflation). 

 #. The economy is considered to predominately be operating at a natural rate of employment.
 #. This rate is determined by supply-side factors in the economy such as the flexibility of labor markets and not demand-side factors.
 #. An increase in demand will cause inflation in the medium term but not a long term increase in employment or output.
 #. A decrease in demand will only reduce inflation and cause no loss of output in the long term. 
 #. Inflation is harmful to long term growth and output.

Therefore in this model inflation targeting is important to long-run economic growth and is best done through suppressing aggregate demand.


#### Adverse effects

High repo rates are the primary tool used by SARB to achieve its mandate of control inflation (@epstein2002employment, p20). However, these high interest rates  may have negative effects and slow down growth(@doi:10.1080/02692170701880775, 246). Local investment has been shown to be interest dependent(@epstein2002employment, 17). Further, high real interest rates also increase government spending because they raise the cost of government borrowing on domestic markets (@epstein2002employment, p 19). In addition, high interest rates can have a distributional effect transferring money from debtors to lenders. This can often be regressive(@epstein2002employment, p19). However, high interest rates are also the result of a high spread between the repo rate and commercial banks' lending rates(@padayachee2019can, 15). Thus, commercial bank regulation may be important in conjunction with changing repo rates in order to lower general interest rates.

### Keynesian approaches to monetary policy

More Keynesian approaches would suggest revising the mandates to focus more on output and employment. This approach is based on several significant theoretical differences. It emphasizes the role of demand in maintaining output and growth levels as well and the importance of active government policy to maintain such demand. Further, Keynesian views on inflation show it as both less dangerous and less directly linked to expansionary monetary and fiscal policy. In these models, the inflation targeting policy of the central bank using high interest rates has hampered growth and employment in South Africa. Here the solution is more flexible monetary policy, coordinated with more controlled exchange rates and possibly expansionary fiscal policy. 

In Keynesian models markets are often volatile and require frequent government intervention to maintain full employment(@costabile2017activist, 1420). Contrasting with more neoclassical views, governments can effectively return markets to high employment without damaging private investment, or drastically and permanently increasing inflation.^[Neoclassical models where expansionary policy leads to permanent increases in inflation are based on ideas that rational consumers will adjust all behavior to account for expected inflation. However, Keynesians argue that many behaviors and contracts in the economy are grounded on nominal terms (ones which do not account for inflation)]. Further, in these models moderate inflation is not harmful to economies and does not cause major instability for countries(@setterfield2006inflation). Additionally, these models do not see inflation as the inevitable result of increasing aggregate demand. Instead, they see inflation as primarily caused by bargaining on the supply side. Finally, these models are based on very different assumptions about the way private investors will react to less orthodox central bank policies. 
They claim there is little evidence that conservative macroeconomics policy in developing economies can secure international investment for growth. They instead suggest that sustainable growth must come from local investment, promoted by government if necessary.


#### Employment targeting

So what do more Kasai views suggest as alternative central bank mandates? They emphasizes that monetary policy and central bank policy must be appropriate to the current and local situations (@epstein2002employment, p 30). Strict targets of any type do not allow for this flexibility (@epstein2002employment, p30). Thus, constant inflation targeting is not appropriate in times of massive unemployment. Further, while the risk of short term political motivations is acknowledged, this view sees the importance that democratic processes should hold in determining the mandate of the central bank(@epstein2002employment, p31). Finally, they see the need for better coordination of macro policy with the national treasury(@padayachee2019can, p14). 

However, this does not mean that controlling prices is not important, just that it must be done in conjunction with other macroeconomic goals(@doi:10.1080/02692170701880775, p 244). To achieve this the SARB will need to use several historical central bank tools recently abandoned as central bank policies have been liberalized(@doi:10.1080/02692170701880775, 244). For example, the central bank should use its historical power regulating other banks to ensure that employment-generating investments are sufficiently financed(@doi:10.1080/02692170701880775, p249). This includes capitalizing development banks(@doi:10.1080/02692170701880775, p 255). A further tool the reserve bank may need for more expansionary policy is a reintroduction of capital controls(@doi:10.1080/02692170701880775, 256). That is controlling the inflows and outflows of money from the country. While minimal capital controls may encourage international investment they give the reserve bank little control over the interest rate. This is because without capital controls a decrease in domestic interest rates may result in significant outflows of money from the country.
However, there are many important questions that must be studied for this approach. This includes the need for more research on the link between monetary policy and employment(@doi:10.1080/02692170701880775,247)



### Reserve bank independence

Central banks have historically been given a set of goals and policies through laws, but been free in how they chose to reach those goals. This has benefits in ensuring that the central bank cannot be used for short term harmful expansionary policy to ensure political goals based on temporary booms. Further, it allows for the central bank to act quickly to avert crises without entering into prolonged political debate(@klomp2009central)  Empirical work has also shown that central bank independence is correlated with lower inflation in many countries. However, many other studies suggest this correlation is not because independent banks keep inflation lower.But rather  that governments who use other policies to lower inflation also favoring central bank independence.

However, since there inception as private institutions reserve banks have built tighter relationships with government. Partially this was done through nationalization. But, mostly it was done through laws allowing the state to control aspects of the central bank's operations from central bank's managers to mandates(@blancheton2016central). 


Towards the end of apartheid in the 1980s, SARB came under increasing control of the nationalist party (@rossow2019independence). The policy they followed while centered around price stability allowed for considerable inflation. Further, its control of interest rates was used by the nationalist government for short term electoral victories. During the transition, independence of the central bank was placed in the constitution. This was in line with more neo-liberal global central bank policies, which saw government control of central banks as leading to harmfully inflationary monetary policies to ensure short term economic gains. Although initially, the ANC government motivated by research from MERG supported less independence from the central bank old nationalist and international political forces pushed them towards accepting central bank independence. This was, in part an attempt to remove power over the central bank from the new ANC government as the nationalist government was worried the ANC would use loose monetary policy to fund a number of more socialist projects and that this would damage key constituents the nationalist party represented, including businesses heavily dependent on international finance and holders of large amounts of assets whose value would be eroded by inflationary monetary policy(@rossow2019independence).

The cooperation of the central bank with the rest of government is essential for successful policy implantation. However, this does not mean that direct government control of the central bank is necessary desirable. In practice many central banks including South Africa's already work closely with government, and a reduction in independence may do little to benefit this while introducing significant dangers of political uncertainty and bargaining compromising the actions of the SARB.

## Conclusion

The mandate of the SARB is a historically controversial one and is likely to be challenged again many times in the future. The current macro economic policy of South Africa has failed to produce sustainable and high growth. However, whether this is due to failures of the conservative policy choices or other factors is the country remains debated. An alternative more Keynesian set of macro economic policies including a revised role for the reserve bank, offer a potential solution to South Africa's economic problems, through a strategy of state led growth. These policies are in contradiction with the global shift in policy towards neoliberal solutions. Because of this, they risk damaging investor confidence both for domestic and foreign investors. However, if such policies successfully addressed some of South Africa's economic social and political ills they may do more to boost investor confidence than to damage it. While these policies have significant risks they must be carefully considered because the alternatives neoliberal economic plans entail severe human costs of their own. Austerity involves cutting back services many South Africans rely on, while attempts to reduce unemployment through wage cuts would lead to conflict with unions and a severe regression in wages and working conditions. 

The private ownership of the central bank may act as some hindrance to coordinating central bank policy with other macro economics policy in South Africa. However, this ownership is largely symbolic. The nationalization of the reserve bank may only damage investor confidence with little benefit. 


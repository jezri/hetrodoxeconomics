# Main

## A summary of Douglas North's remaking of economics history by Dimitris Milonakis and Ben Fine

```{.biblatex}

@article{milonakis2007douglass,
  title={Douglass North’s remaking of economic history: A critical appraisal},
  author={Milonakis, Dimitris and Fine, Ben},
  journal={Review of Radical Political Economics},
  volume={39},
  number={1},
  pages={27--57},
  year={2007},
  publisher={Sage Publications Sage CA: Thousand Oaks, CA}
}


```

North catalysed an analysis of institutions across social sciences in ways ranging from neoclassical economics to narratives. He first used cliometrics (analysing history through neoclassical economics), focusing on price systems allocating scarce resources. Next he looked at how private property(ownership and enforcement) affected the efficiency of these mechanisms. So institutions were needed to protect private property against self interest. These institutions can sometimes be changed by ideology which is itself a prodcut of self interest. Finally North moves to explaining ideology in a cognitive way. However he was unhappy with his theory of institutional and historical change.

We will focus on Norths work focusing on market imperfections and non-market responses to them.

### Theoretical considerations 

In this writing North tried to explain geographical and temporal differences in economic performance. Here institutions and institutional change are central. North found no existing theory adequately explained *"the dynamics of change in economic history"*. He found old institutionalism lacked coherent theory and thought Marxism was too deterministic, though it included many elements missing from neoclassical economics. 

North liked neoclassical economics focus on scarcity and competition and its methodological individualism, but it did not explain large-group behaviour and the stability this caused. Because (potential free riding) in a neoclassical theory would hinder such large-group behavior; homo-economicus should not obey laws. So neoclassical economics cannot explain history without many non-market elements. To patch up neoclassical economics North adds some new elements. He changes the rationality postulate, adds institutions and uses transactions costs to link costs of production with institutions; *"when it is costly to transact institutions matter"* . Here political forces explain differences in economic performance and explain inefficient markets.

Instatutions:

: *"Humanly devised constraints that shape human interaction"*

Institutions key role is reducing uncertainty by structuring life and limiting the choices of individuals. North modeled relative prices changes as incentivising institutional change. However enduring inefficient institutions disproved this model so North tries to explain these. Further he tries to find another theory of institutional change and to find an explanation to the free rider problem. He basis this on three instatutions

* Property rights
* The state (to enforce property rights)
* Ideology

North claims well specified and enforced property rights explain the growth of western economies in the modern era. However he says inefficient property rights can also be exist supported by the state. He models states as rulers maximizing their own welfare under the constraints that they must compete with other rulers to maintain their position and that efficient laws may be costly to enforce. The state puts its own welfare and survival above efficiency so inefficient property rights persist.

North than uses ideology to explain the relative lack of free riders. The state and ideology can explain almost anything that happens especially because they are defined loosely as is their relationship to each other. Still North relies mostly on neoclassical tools: methodological individualism, rational choice and comparative statics. He departs from neoclassical economics in ways which complement it, fixing its deficiencies.

North talks often about change but social and institutional change are only incremental in his model and driven by changing relative prices. This model is  a comparative static one driven by individuals not a process of change. North admits revolutions are possible but downplays their historical importance.

### Making history

Because Norths theory is abstract it connection to history is dependent on what history he includes. This choice is made to expand neoclassical economics. So population changes become a basic explanatory variable because *"it complements transaction costs, the power of the state, and transhistorical notion of competition"*. The complexity of history cannot be collapsed to a handful of concepts, so North amplifies the effect of the select elements drawn from history. 

North argues that populations generally grow and come against barriers creating tension between population and resources. This is alleviated by technology. If population growth outpaces technological growth there are decreasing returns.

Technological growth:

: *"the growth rate of knowledge and its application"* which is determined by property rights.


North gives to explanations for the beginnings of settled agriculture.
 
 #. A choice made by a band of  hunter gathers to increase their marginal product of labour by farming.
 #. Diminishing returns led to efforts to slow population growth and tribal groups competed to exclude each other from land. This led to "exclusive communal property rights". Thus increased the private rate of returns for knowledge about the resource base.

Population growth was the largest force in ancient history for North. Malthusian crises drove economic organization in ancient societies including developments of individual property rights, slavery and the state. This increased productivity causing sustained economic growth. Exclusive communal property rights lead to increased specialization. This makes collective decision making difficult leading which is reduced by the state. 

Improving state organization from Egypt to Persia to Greece to Rome reduced transaction costs expanded markets and increased regional specialization.

### Summary of remaining paper (To be revised)

North claims all none market structures such as serfdom ect are done in order to reduce transaction costs due to lacking markets. However this does explain actual history in which serfdom remained in spite of large markets in much of Europe. Further there is little historical evidence that serfdom is was at the time more efficient than renting land for money (rather than labour). Finally it is not useful to describe any none market institution as existing because there wasn't a market, it fails to explain the specifics of the institutions which arose. 

Other attempts at historical institutionalism focus on homo socio-economicus or human actions shaped by social and historical forces. Alternative approches also focus on ideology as a mechanism for class conciousness to coordinate common action and form common goals. Further simple aggregation of individuals fails to explain the more complex interactions between them.





















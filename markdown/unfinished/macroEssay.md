# Main

<!--
The deadline is Thursday 30 April 2020, 4 pm.

Consider the following statement:

		According to the classical approach, an increased desire to save leads to an 	increase in investment, via the interest rate mechanism, with the level of 	income remaining unchanged. In Keynes, however, an increased desire to 	save leads to a fall in the level of income, and this is generally referred to as 	the Paradox of Thrift.							

With the above statement as background, write an essay on the implications of an increased desire to save. Present both the classical view and the Keynesian one.
	[70 marks]

Illustrate the above effect of an increased desire to save by means of an IS-LM diagram. Your answer must consist of both a diagram and an essay. Your diagram must indicate what curve shifts, and your essay must explain what is happening in the diagram. 		[20 marks]

Your assignment must contain the list of references you used in this essay. In your text you should refer to the references you used in writing your essay.	
	[10 marks]


References to be used

As a minimum, consult the readings by Hagen, Perlman and Johnson in the course outline. 

Hint 1	In discussing the classical approach you must make use of the type of diagram employed by Hagen. Your discussion must include an analysis of a shift in the savings curve in Hagen’s diagram.

Hint 2	In discussing Keynes’s approach you must make use of the type of diagram employed by Johnson in his Figure 2. Your discussion must include an analysis of a shift in the savings curve in Johnson’s diagram.

Hint 3	Read the first two pages of chapter 16 of Keynes’s General Theory.

Word length for essay: 3 000 words.
-->


## Introduction (500 words.)

## Classical model of demand and supply for money and determination of interest rates. (500 words)

The simpleir classical model of supply and demand is derived from a bartter economy. 
This is becouse initially classical economists thought that money did not distort ecomonic forces significantly but only facitilted trade. However the more detailed version of the neoclassical argument derived in a monied economy is the one Keynse was argueing against. Therefor it is this model I will discribe in this easay.

We begin this analysis with a simple model of the circular flow of reasources and money in the economy. Here households sell various factors of production to firms and buy finished products from firms. Households spend all thier income on finished proiducts and firms spend all thier revenue on the factors of production. (This comes from an anayisis were rents as well as profits are considered as going to factors of production).
      
      
This model is than complicated by adding several additional features to the economy and showing that the monetary flows in out of these new sectors is balanced in the classical model. This is done for the financial sector, goverment, the international sector, and the potential build up of inventories.

### Inventories

Is is possible that shop owners may keep infantaries either becouse they believe they prices will riase later or simply to maintain sufficent stoke incase of unexpected increases in demand. If inventories are rising than production is greater than consumption and if inventories are falling than inventories are below consumptions. In the classical model however buisness owners keep inventories roughly stable for most of the time. This resource inflows and outflows to industries are equel and so the net consumption is equel to proiduction again. 

### Financial instautions

Another compilacation added is that of saving and borrowing from fincail instautions. In this classical model the rate of inte rest is determined by the rate of profit. As all investment returning profit above the rate of profit will be borred to be invested in. This investment is highly depdent on interest. Thus an increase in savings will always result in a decrease in interst rates which will result in an equel increase in investment from firms. Thus the inflows and outflows of money from the financial sector are allways equel in the classical model.

### Governement 

Here again we equate tax revenue with goverment expenditure. Goverment borrowing or paying back debt is handeled in the fincail markets and results in either increasing or decreasing intrest. Thus in this model goverment deficits drives up interest. This crowds out private investment. 

### Stabilty vs full employment

Here we have decribed a stable model of a clsoed economies where the circular flow of money is kept at a constant level. However there is nothing in this model which sets that this must be the maximium amount that the economy could produce at full employment. This stems from another part of classical theory namly the theory of how the earnings of each factor of production equels its relativet productivity. Thus labour is employed up until wages equel productivity and this is an exogious variable in this model which the forces of agregate demand cannot change.

### The implications of savings in the classical model

In the classical model there is a direct trade of between consumption and investment as the economy is oprtating at full employment. An increase in savigns will lower interest rates and increase investment. This has implications for how income should be distributed in an economy. As wealtheir people tend to save more it implies that for high investment and consiquent growth income may need to be distributed more regressivly to ensure sufficient savings.

## Keyens hicks and the ISLM model

This is a model of how output is determined often below full emplyment by a combination of the goods labour and money market equilibrium.

## Fractional reserve banking and money as credit. (500 words)

One major issue with this model is its modeling of the financial sector. As fractional reserve banking allows lending to be a significant multiple of the reserve saved in the bank. This means that rather than lending and borrowing needing to balance for stable demand instead the repayment of loans must be ballanced with the issueing of new laons to keep money supply and demand constant. 

## Inflows and outflows of lending matter more than savings. (500 words)
    
## Savings reduce consumption (500 words)

If we look at hicks ISLM model an increase in savings will lower consumption at any given rate of interest in the goods market. 
This will reduce result in a downward shift of the IS curve while the money market remains constant.


```r
x <- (1:100)/10
IS <- 10-x
IS2 <- 9-x
LM <- x
plot(x,IS,type="l",main="IS-LM",sub="Result of increased savings",xlab = "Output",ylab="Interest")
lines(x,LM,col="red")
lines(x,IS2)
```

![plot of chunk unnamed-chunk-1](figure/unnamed-chunk-1-1.png)

This results in an lower equilibrium output at a lower interest rate. The net result may increase or decrease investment depending on how sensitive investment is to changes in interest vs how sensitive it is to changes in aggregate demand.

## This decreases aggregate demand (500 words)

THe result of this keysian model is the paradox of thrift. That the more a nation collectivitly saves the worse of everyone will be in future periods.

## Conclusion

Keynesain economics disporved many of the assumptions of the classical economic models and became a central part of economic thoery and policy making after world war two. However following the global rise of neolibrelism keysain ideas became less popular being replaced with more monetarist ideas. Whether this is due to empirical flaws or due to the political inconvience of the more labour and wellfare friendly conclusions of Keysian ideas remains debatable.

        

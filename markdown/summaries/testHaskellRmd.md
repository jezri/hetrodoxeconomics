---
output: markdown
title: R Notebook
---

# Main

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you
execute code within the notebook, the results appear beneath the code.

Try executing this chunk by clicking the *Run* button within the chunk
or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*.

``` {.r}
plot(cars)
```

![plot of chunk
unnamed-chunk-1](../pics/384fbb3845af723f97295215b55e9084be8ff190.png)

``` {.r}
names(knitr::knit_engines$get())
```

    ##  [1] "awk"       "bash"      "coffee"    "gawk"      "groovy"    "haskell"  
    ##  [7] "lein"      "mysql"     "node"      "octave"    "perl"      "psql"     
    ## [13] "Rscript"   "ruby"      "sas"       "scala"     "sed"       "sh"       
    ## [19] "stata"     "zsh"       "highlight" "Rcpp"      "tikz"      "dot"      
    ## [25] "c"         "fortran"   "fortran95" "asy"       "cat"       "asis"     
    ## [31] "stan"      "block"     "block2"    "js"        "css"       "sql"      
    ## [37] "go"        "python"    "julia"     "sass"      "scss"

``` {.haskell}
putStrLn "test"
```

    ## Error in running command /usr/bin/ghci

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or
by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output
will be saved alongside it (click the *Preview* button or press
*Ctrl+Shift+K* to preview the HTML file).

The preview shows you a rendered HTML copy of the contents of the
editor. Consequently, unlike *Knit*, *Preview* does not run any R code
chunks. Instead, the output of the chunk when it was last run in the
editor is displayed.

# Imports

import package: (hetrodoxEconomics) document: (summaries.sraffaSummary) as: (sraffa)
import package: (hetrodoxEconomics) document: (summaries.DouglasCriticSummary) as: (north)

# Main

## Sraffa

### ref(sraffa:Summary of On the relation between costs and quantity produced)

## Institutional economics 

### ref(north:A summary of Douglas North's remaking of economics history by Dimitris Milonakis and Ben Fine)



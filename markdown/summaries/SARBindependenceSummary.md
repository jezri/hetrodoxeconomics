# Main


```{.bibtex}

```

## Abstract 

We will look the political reasons South Africa secured the independence of the Reserve bank during the transition to democracy. 
This coincided with global trends. Negotiations over economic issues were overshadowed by political concerns. The disorganized discussion between major political actors about transition economics is displayed in the debate over central bank independence. Here the late apartheid governments favored a market orientated view while the ANC initially did not want full constitutional independence.
In discussions central bank independence and a mandate of price stability were agreed upon. 
We examine an interview on the debate with former President FW de Klerk  and former central bank governor Dr Chirs Stals.

But 25 years later the ANC is questioning the independence of SARB. In 2018 the ANC stated intentions to nationalize the SARB. The exact motivations for this is unclear. However investors and credit rating agencies see it as a potential first step to extending executive control over SARB. 

## Introduction

The actions of SARB, including their monetary policy in the 1980s are still controversial. 


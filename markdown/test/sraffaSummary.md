---
output:
  html_document: default
  word_document: default
---

# Main

## Summary of On the relation between costs and quantity produced

``` {.bibtex}

@article{sraffa1925,
  title={On the relation between cost and quantity produced},
  author={Piero Sraffa},
  year={1925}
}
```

Most neoclassical economic textbooks (circa 1930) describe the economy
as constituting of industry with constant, increasing and decreasing
returns to scale (RTS), *ceterius paribus*. Economists often link
agriculture with decreasing (RTS), manufacturing with increasing RTS and
industries based on unassisted labour with constant RTS. Some recent
writers disagree, generally leaving agriculture with decreasing RTS, but
claiming other industries classification depend on their *"particular
circumstances"*. These other industries require either statistical
measurements or detailed models to classify them. If such classification
proved impossible it would undermine much current economic research
based on speculating on the implications of classifying different
industries in various categories.

However it is unknown if this lack of classification is due uncollected
data and insufficient economists or if industries cannot be divided by
returns to scale. That is can both increasing and decreasing RTS act on
the same industry concurrently so that classification is done only by
arbitrarily defining the boundaries to the industry and selecting a time
frame(long or short run).

Classical economists always related decreasing productive with land
rental, and so discussed its effects on distribution. In contrast
classical economists linked increasing productivity with division of
labour and so only discussed its effect on production not on
distribution. Only recently, economists have unified these into a *"law
of non-proportional productivity"* which is one of the determinant of
price. Historically there was little evidence for a connection between
output and cost so older economists connected division of labour with
progress and not with quantity produced and studied only the
distributional effects of decreasing productivity of land. Further
decreasing RTS for agriculture could not explain relative price changes,
because almost all goods depended on agricultural outputs.

Theories of RTS emerged indirectly as a result of utility based theories
of value. These theories included decreasing marginal utility and a
demand curve with a negative relationship between quantity consumed and
price. This inspired a symetrical supply curve linking quantity produced
and costs. If returns to scale were constant demand would have no effect
on price which would be dictated solely by the costs of production.

### II Increasing costs

An increase in one factor of production *ceterius paribus* leads to a
less than proportional increase in output which is known as the law of
diminishing returns. However an increase in the overall size of output
with an increase in all factors can lead to greater than propionate
returns. Thus there are not two separate laws of diminishing and
increasing returns for different industries. In general constant factors
can be decreased just not increased.

Here is an example from Tugot gives

> Farm land is more productive the more it is tilled with increasing
> returns. Therefor if market size increases the cost of production will
> go down.

But if this were true why does the farmer not supply smaller quantities
by only tilling a portion of their land? I.E "Use the amount of land
which cultivated to its maximum productivity yields the product
required". Let $Q_1$ be the qaunitity the farmer can produce using all
land cultivated to maximum productivity (assuming uniform land quality).
Then, for all output levels below $Q_1$ the farmer will produce like
this. Thus for all $Q \leq Q_1$ productivity is constant, as a change in
output requires a proportional increase expense, (for additional labour
and capital used to cultivate additional land).

I cannot make complete sense of the graph, the wording is ambiguous.
Figure one illustrates production under Tugot's model where inputs
(labour and capital) are used evenly over a whole piece of land. The X
axis represents the amount of input used on the land. The Y axis
measures output per unit input or productivity.

![plot of chunk
unnamed-chunk-1](../pics/8d40090f2f4db06ebbe3321330112087b9a3b1c3.png)

The marginal productivy is the additional is the amount of output added
from the last unit of intput added.

The avererage productivity is the total amount produced devided by the
total input.

$$\text{average product} = \frac{1}{\text{Input}_1}\times\int^{\text{Input}}_0 \text{marginal product} d \text{input}$$

$$\implies \text{average product}\times{\text{Input}_1} =\int^{\text{Input}}_0 \text{marginal product} d \text{input}$$

And this has geomterical intutions.

$M'$ is the point of maxiumium productivty.

Becouse all the land homongenious a similiar graph shows productivity
for any fraction the land all with the same maxiumium productivity.

This models the constant factor as "indefinitely devisible". Often the
avaible quanity of this factor is greater than the minimium needed for
production so there is a range where the quantity can be easily vaired.
Possibly reducing the quantity of this factor beyond a lower bound will
reduce producitivty more than using a smaller amount of other factors
with an the orignal quantity of the constant factor. In poractice this
lower bound is small relitive to the size of the market. This is the
important measurement of size becouse factors are fixed at the level of
the market and not at the level of individual suppliers.

Really, increasing producitivy is common for firms at least at low
output, but becouse of other mechanisms. Modeling this mechanism rising
productivity from increasing $A$ with $B$ constant implies *\"negetive*
productivity of B, where A and B are factors of production. So,
increasing B with while holding A constant after a point would have a
negative marginal product and reduce total product. For example if land
use increases beyond some quanity while capital is held consant
production will fall.

Assuimg factos are used in the best way, no farmer would use more than
this quantity of land "even if it were free". Becouse the best way to
use more land is not to use it. So increasing productivity from poor
ratios of factors is only possible when there is an excess of a factor
which cannot be costlessly removed.

If hypothetically decreasing productivity is linked with the proportions
which firms use to combine factors of production, we should see if a
common cause producers this effect in different industries. Many
economists believe such a common cause exists at least to some extent.
This thoery assumes

1.  The criterion for economic choices?
2.  varied and indepedent parts of the varilable factors, parts of the
    constant factor, or production methods.

Given these assumptions a profit maxiumizing firm will order the varied
input factors and production methods and produce lower quatities with
better combinations substituting worse combinations when better ones are
exhasted.

The alternative hypothisis is that each industry has a independent law
of diminishing returns. Difficulty finding an industry with no possible
substiution left makes it hard testing the extent to which deminishing
returns are industy specific. However if non-economic forces determined
the productive combinations firms used they may not follow a decreasing
order of effieciency.

We will examine a model for this argument in the argicultural sector.
Originally the argument for technical conditions in the sector was
linked to simple models of decreasing productivity of labour as land is
held constant (isn't this just the general rule?). This implies that
through some mechansism unknown to economists the marginal product of
each additional input is decreasing. A more complex model yeilds
different results.

A farmer, after investing 1000 lires a year on cultivating a farm may
choose from a wide varity of different ways to to invest another 1000
lires. They could spend on seed or fertalizer or live stock or any
combination. Further differnt investment will yeild different
(hetrogenious) products which must be compared on thier value so the
farmers choice will be made on economics and not purely technical
grounds. Choices of some methods make others impossible to persue in the
future, If the productivness of successive choices is independent than
the farmer must spend money on less and less rewarding investments after
purchasing the more useful ones.

But what happens if after one investment the productivity of others
increase for example a green house allows farmers to make better returns
on seeds. This is simialar to the above discussion of input to land
ratios. ie above a certain small quantity why not buy increasing
investments in a combination they can be used best in already for
example a small green house and a few packets of seeds. Consiquently in
general firms would be expected to buy well combined bundles each of
which yeilds diminishing returns. However,capital productivity is often
very interelated and capital may be relitivly indivisble, (whats half a
tractor?)

This thoery is critized by Wicksteed who offers an alternative model. He
divids productivity curves into descriptive curves and functional
curves. One is linked with phyiscal and the other with economic laws of
diminishing returns.

Desciptive curve:

:   The decriptive curve is formed by ploting the product various
    different hetrogenious parts of a factor of prodctuion will produces
    with constant other factors of production against the quility of
    parts (measured ordinally and ranked by output). This is linked to
    Ricardian diminishing returns.

Functional curve:

:   For a given part of some factor of production plot output against
    value of input. The last unit of input for which this is profitbal
    is called the marginal unit and measures the distibutive share of th
    input?

However this distiction appears false. Any gernerally decreasing curve
must be discriptive. Wickseed claims that the marginal yeild not upon
the nature of the marginal input but only on the total amount of input.
Hoever if indentical marginal inputs result in differeing marginal
output then they must be used differently so marginal productivity
depends on the use of marginal inputs. If all previous inputs are in
best use then there are only equally or less productive uses for the
marginal input. The larger the input the lower we descend the hierarchy
of possible uses. So the functional curve shifts the "'difference in
nature' and there the 'arbitrary ordering'" from the inputs to thier
uses. Further this pattern of usage of parts of heterogeneous factors is
not arbitrary but ordered by the produce to maximize profits.

Another objection is that heterogeneous parts of a factor cannot be
ordered by their productiveness consistently with changing combinations
with other factors. This would imply constructing a static curve of
diminishing returns ordered by fertility of the pieces of land is
impossible. Marshall faults Ricardo on this idea. Many early economists
considering the problems as one of ranking fertility of land for
agriculture have numerous definitions of output per area per unit
capital per unit labour ect. We need a criterion for which pieces of
land to cultivate and to what degree to produce any given output at
minimum cost. The piece of land which has the greatest productivity
where average productivity is maximized would be the best choice. In
practice this would mean every new piece of land is cultivated at least
until it reaches maximum average productivity before the next piece is
cultivated. The alternative would yeild lower marginal returns. This
order does not change with intensity of cultivation. (However weather is
well know by farmers seem another problem)

Barone develops a faulty model of perfect market acting with diminishing
returns. Here different firms operating at different costs represent
different production options with decreasing returns, at low prices only
low cost firms survive and as prices rise less efficient firms join the
market to increase output at higher costs. Thus the cost of the marginal
firm equals the market price and competitive markets have increasing
costs. This model is similar to Ricardo's rent model however it fails
because of differences between the two markets. Agricultural production
expands by using the worst land but industries may expand by attracting
efficient firms from other industries. Firms may vary between similar
industries depending on profits. (If you include the opportunity cost of
production as a costs does this model work again?). Even destroyed firms
or newly formed ones may be made from components labour and capital that
flow between industries and this flow may be of the most easily
transferred not the least efficient components.

> **Example**
>
> -   in industry $\alpha$ firm a makes a profit ($\pi_A =20$) and firm
>     b a profit ($\pi_b= 10$) (so firm a is the more efficient)
> -   if they switched to industry $\beta$ then $\pi_A=18 \quad \pi_B=5$
> -   If profits fell in $\alpha$ so $\pi_A=10 and \pi_B=6$ then À the
>     more efficient firm would leave industry A

This model is like growing a single crop not the use of all farming
land. A farmer will probably grow a new crop on cultivated land that was
used to grow a less profitable crop. So comparative costs not
diminishing returns shape production.

Supply curve:

:   A curve representing every combination of price and the quantity the
    industry will produce at that price.

One way to construct the curve is treat the industry as a firm using all
of a constant factor and increasing other factors until demand is met.
The marginal cost (cost of the unit produced least efficiently) is the
price needed to produce that quantity. *"The collective supply curve in
conditions of increasing cost represent the marginal cost"*

However, this overlooks that general equilibrium results from a
collection of individual equilibria that competing firms reach
independently. So the construction must be an aggregation of firms
supply curves.

Supply under diminishing returns looks like demand under decreasing
utility so one may try construct it identically. Community demand is the
sum horizontal of individual consumer demand, because decreasing utility
from one consumers is indepdnent of the consumption of another consumer.
However diminishing returns operates for the industry as a whole not
individual firms. Individual firms face near constant factor costs for
factors fixed at the industry level and may even have economies of scale
as they grow not outweighed by scarce factors (arn't economies of scale
incompatible with earlier models). But these cannot be added up as the
supply of one firm is relient on holding the production of other firms
constant. We need to move the increasing costs from the industry to the
firm in order to add these up. We do this by fixing the number of
producers and fixing the amount of the fixed factor each producer uses,
this allows the curves to be aggregated as they are independent.

### III Decreasing costs

Firms often decrease cost pre unit as they increase production. This has
two causes.

1.  Internal economies of scale for example division of labour, (not
    including better rations of factor use) This causes a decrease in
    marginal costs which results in a decrease in average costs.
2.  Firms have fixed costs, (overhead) increasing less than
    proportionality with output because their cost is spread over a
    larger output. These only decrease average cost not marginal cost.
    Incorrectly one may treated fixed costs as a constant factor and
    variable costs as a variable factor, but for fixed costs only
    average costs decrease where as with constant and variable factors
    marginal costs decrease as constant factors are better used.

Decreasing costs is anathema to free competition. A firm without
diseconomies of scale at output below the market demand would get the
whole market. However some economists economies of scale decrease costs
under competition. (There's more here but is seems besides the point)

Equilibrium price theory for individual commodities is based on
decreasing costs with decreasing returns from heterogeneous factors and
factor combinations

#### Marshal's equilibrium price theory

Following is a description of Marshal's model.

-   Division of labour leads to decreasing costs.
-   Division of labour depends on factory size. (But he incorrectly
    incorpates this into an analysis of competition)
-   Division of labour can form between many small factories.
    -   This helps with vertical disintegration with many small tool and
        machine makers
    -   So division between small firms requires geographical proximity

These external economies of scale are not considered as important as
internal ones, particularly because of locality constraints. This would
imply changing costs always corresponds with localization and dispersion
of industry.

Improvements in technology are not considered external economy of scale
because they result from general scientific and engineering progress
exogenous to industry size.

Internal economies of scale were incompatible with so he swapped this
model and expanded his model of external economies of scale to explain
decreasing costs under competition.

In *Principles of Economics* Marshel based his theory of value on this
model. But the theory of value is now used with the model changed.
Marshel swapped models by presenting the new model as a common
reconciliation of competition with decreasing costs, albeit relaying on
external economies of scale without empirical or theoretical reason.

##### Marshels new equilibrium price theory

Industry supply is derived as follows:

-   Individual firms equilibrium depends
    -   Their costs
    -   The quantity they produce
    -   The quantity other firms produce
-   Free competition implies that
    -   each firms is a small proportion of the industry that their
        output variation has a negligible effect on price.
    -   each industries is a small proportion of the economy and their
        input variations has a neglible effect on factor prices.(why?)
        so industries can increase factor use at constant costs.

###### Supply of a firm

We plot unit cost against output. There cannot be increasing costs for
all units output as this would make all firms infinitely small as each
firm must cut its size to compete. There cannot be decreasing costs
indefinitely as this would result in monopoly.

### III Decreasing costs

Firms often decrease cost pre unit as they increase production. This has
two causes.

1.  Internal economies of scale for example division of labour, (not
    including better rations of factor use) This causes a decrease in
    marginal costs which results in a decrease in average costs.
2.  Firms have fixed costs, (overhead) increasing less than
    proportionality with output because their cost is spread over a
    larger output. These only decrease average cost not marginal cost.
    Incorrectly one may treated fixed costs as a constant factor and
    variable costs as a variable factor, but for fixed costs only
    average costs decrease where as with constant and variable factors
    marginal costs decrease as constant factors are better used.

Decreasing costs is anathema to free competition. A firm without
diseconomies of scale at output below the market demand would get the
whole market. However some economists economies of scale decrease costs
under competition. (There's more here but is seems besides the point)

Equilibrium price theory for individual commodities is based on
decreasing costs with decreasing returns from heterogeneous factors and
factor combinations

#### Marshal's equilibrium price theory

Following is a description of Marshal's model.

-   Division of labour leads to decreasing costs.
-   Division of labour depends on factory size. (But he incorrectly
    incorpates this into an analysis of competition)
-   Division of labour can form between many small factories.
    -   This helps with vertical disintegration with many small tool and
        machine makers
    -   So division between small firms requires geographical proximity

These external economies of scale are not considered as important as
internal ones, particularly because of locality constraints. This would
imply changing costs always corresponds with localization and dispersion
of industry.

Improvements in technology are not considered external economy of scale
because they result from general scientific and engineering progress
exogenous to industry size.

Internal economies of scale were incompatible with so he swapped this
model and expanded his model of external economies of scale to explain
decreasing costs under competition.

In *Principles of Economics* Marshel based his theory of value on this
model. But the theory of value is now used with the model changed.
Marshel swapped models by presenting the new model as a common
reconciliation of competition with decreasing costs, albeit relaying on
external economies of scale without empirical or theoretical reason.

##### Marshels new equilibrium price theory

Industry supply is derived as follows:

-   Individual firms equilibrium depends
    -   Their costs
    -   The quantity they produce
    -   The quantity other firms produce
-   Free competition implies that
    -   each firms is a small proportion of the industry that their
        output variation has a negligible effect on price.
    -   each industries is a small proportion of the economy and their
        input variations has a neglible effect on factor prices.(why?)
        so industries can increase factor use at constant costs.

###### Supply of a firm

We plot unit cost against output assuming industry output is constant
here unit costs include normal profits. There cannot be increasing costs
for all units output as this would make all firms infinitely small as
each firm must cut its size to compete. There cannot be decreasing costs
indefinitely as this would result in monopoly.

![plot of chunk
unnamed-chunk-2](../pics/40d247c8339afca00467caa7961dc17b454f567e.png)

This implies that the curve has a miniumium where production has least
cost per unit which is also the minium average cost. Varying industy
output $z$ changes this curve. Each $z$ will result in a market price
which competitve firms must sell at, so the demand each firm faces is
horizontal. The demand curve must be tangential to the supply at the
point of maxium economy. If the supply curve did fall below demand this
would result in supernormal profits but whatever allowed this reduction
in costs would be a factor of production and so these profits could be
accounted for as increased factor renumeration missed out by the cost
function. If we include it we return to a model where total revenue
equels total expense.

However determinants of the price must not be included in these
determinants of the cost. **(This seems wrong to me. For example if
price of wine went up the value of land suitable for growing grapes
should also increase)**. If there is a constant factor of production
used mostly by industry its renumeration would be the effect not the
couse of of the price. Thus the renumeration would represent surplus or
rent. (**Sraffa claims these access to fixed factors cannot be put into
the production cost but I don't see why. It seems to me a confusion of
accounting costs with oppurtunity costs, though in complete markets I
assume these should match**). In Marshel model however all factors of
models are assumed to be variable for indivdaul industries using only a
small portion of economy wide fixed factors. If factor renumeration from
an indiviaul industry is fixed it cannot be rent.

It is possible in this model for marginal costs and consiquenlty average
costs to remain constant over a range of production so no unique
equilibrium exists. Pigou solves this by extending marshel model
assuming that firms will maximize production until profits decline.

Without external economies individual firms production would be
independnet of $z$ so the supply curve could be modeled by summing
indivdual firms supply. However as different $z$ would result in
difffent firm production curves this aggregation is meandingless.

**Heres is how we find industry supply**

For each $z$ there is a unique equilibrium quanity for the firm in
equilibrium so if we take only price at that quantity we get the the
cost is a function of the firms output and the industries output. (Also
the firms output is a function of the industries output).

![plot of chunk
unnamed-chunk-3](../pics/6e85913bed4a367e4230126c922c1d4cb7e918ac.png)

So we can plot a firms production against $z$

![plot of chunk
unnamed-chunk-4](../pics/07681774a4afa9ff0b62f0ce3c30ffcdfcd2155b.png)

![plot of chunk
unnamed-chunk-5](../pics/cce26fc5e06560b9190da36f0a989e8ff2924c98.png)

Each firm both in the industry and as potential entrants have such
curves and summing such curves at any given price gives total output.

So the supply curve shows the average cost of all firms for any level of
output. (Is it valid to treat total output like this)

If industry average costs are decreasing with volumne then industry
marginal costs are below industry average costs. From the above
derivation firms marginal costs equel the industry average costs. So the
industries marginal costs are below firms marginal costs. This is
becouse a single firm raising production also raises production from
other firms (via external economies of scale). There is a proof that
perfect competion does not maximize collective utilty which derives from
this. It is not rewarding for any producers to expand production beyond
a point even this expansion would lower industry costs enough to
compensate thus price is above collective marginal costs.

### IV Constant Costs

Both reasons for increasing and decreasing costs can act on the same
industry resulting in constant costs. Alternative neither may be present
in an industry which is quite probable. However most econoimsts only
consider constant costs as being an improbable balance of existant
mechanism for increasing and decereasing costs.

### V Co-ordination and critques of the three tendencies

We will now attempt to understand the interaction of these tendiecies to
yeild supply curves for industries with non-protortional costs.

This is difficult becouse the different tendnecies were studied under
different assumptions. Studies of diminishing returns focus on
distribution and those of increasing returns on price and quanitty
produced.

Which tendencie is associated with an industry is linked with how we
define an industry. If we define the industry to include all uses of a
limited factor of production, i.e the iron industry it has decreasing
returns. If we look at an industry that uses a small percentage of any
factor of production for example the clothing industry we get increasing
returns. Further in the short run diminishing returns are more likely
and in the long run increasing returns.

The most serious problem with the thoery is that both supply and demand
determine price are inherent in both seperate models of decreasing and
increasing returns seperatly. A supply curve holds demand and production
of other comodities constant. So the supply curve is valid only for
small changes as large changes would result in changes of these other
conditions.

This range is very small given conditions of increasing costs. They are
constant only when all of a factor is used to produce a single commodity
(why? A variable can be held constant even if it changes it must just
not change as a result of a varible modeled). Thus we can only find the
supply curve for all commodities using a constant factor but a single
comodity cannot have a supply curve with increasing costs.

If we are look at supply for a small number of comodities and one of the
industries increases production than they use a common factor and the
costs of all industries will rise. So production of other industries
using the same resource will not be constant. So we cannot derive a
supply curve. For example an increase in demand for corn leads to more
land being used to grow corn which leads to an increase in the cost of
other agricultural products. This couses and a second increase in demand
for corn as the price of substitues rises.

If the number of industries using a factor is large than an increases in
production from a single industry will have neglible effects on the
factor cost. This would contradict increasing costs.

We cannot constiently consider the effect only of increasing cost only
on a single industry.

Barone developed the same conslusion on a differnt incorrect argument.
Prices are a function of the quantity produced of a whole collection of
products thus no individual supply curves can be derived. Other
econoimcst critized this claiming that it implied demand to could not be
usfuly derived and one could not thus use only demand and not supply.
However the degree of substution and number of subsitue goods is much
smaller. Consumption of one good seldom decrease utility gained from a
whole cross section of goods in the way production of a good decreases
productivity of a large section of industries.

However assuming this away is more costly in Sraffa's model as the
increasing in cost from other industies is likely to be about as high as
in the industry increaseing production.

Further for the case of increasing returns economies of scale external
to the firm must be largly internal to the industry to justify keeping
other production constant. As most economies of scale external to the
firm are transport and communitcation infastructure, and possible
developed financial markets ect these are unlikly to be internal to an
industry. Thus an increase in production of one commodity may lower
costs and increase production of a host of other industry making static
equilibrium improbable.

If an industry is small enough that it does not have economies of scale
extending to other industries it can have only negelible external
economies of scale in its own industry, (though division of labour
between firms seems concievable).

**This shows that stable equilibrium under competion cannot be derived
in a Mashelian way under increasing or decreasing cost structures and
thus is compatible only with constant returns to scale**

If we look at supply for a large number of industries using a common
factor

# Sraffa critique

Sraffa criticises the *certerius parabus* approach in Marshellian
analysis of supply, as being inconsistent with increasing or decreasing
returns to scale. This is because things causing increasing or
decreasing returns to scale, are likely to have effects on the costs of
production of other goods of roughly the same size as of the original
good. Decreasing returns to scale result from the use of less and less
productive pieces of a heterogamous factor of production, this factor of
production is often used by other industries as well so if the more
productive pieces of the commodity are used by industry $A$ then the
marginal cost of producing $B$ will rise by a similar amount to the
marginal cost of $A$. Increasing returns to scale are over a large
quantity of production have two explanations. It could be due to
internal economies of scale but this would lead to monopoly so
neoclassical economies instead tried to explain it with external
economies of scale. However external economies of scale are often due to
factors which effect multiple industries in very similar amounts for
example better transport infrastructure markets and financial
institutions. But this again would lead to an increase in production of
good $A$ significantly altering the cost of good $B$. Finally if good
$B$ is a complement or substitute of $A$ this will further shift $A$s
demand curve.

For example an increase in demand for corn will increase the price of
production of many other crops which may yield a further increase in
demand for corn as the price of its substitutes rise. This critique does
allow for monopolies with increaseing returns to be analyzied with
neoclassical techniques but in general makes Mashelian analysis of
individual markets meaningless due to the significant effects of
substitutes of production.

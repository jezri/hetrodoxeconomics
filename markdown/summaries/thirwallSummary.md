# Summary of **The balance of payments constraint as an explanation of international growth rate differences**.


The neoclassical explanation for differing growth rates across countries focuses on supply side. 
This analysis typified by Denison and Maddison uses production functions. Given a functional form the causes of output growth are divided into labour growth and capital growth .The reminder is then attributed to productivity growth. 

This approach does not explain why factor and productivity growth differ between countries, but a Keynesian approach may.
Keynesian believe demand has a central role in determining output.
So to explain differing output growth rates one must explain differing demand growth rates between countries.
Thus we must explain why different agents, government in particular cannot expand demand.
The largest constraint in an open economy is the balance of payments. This brings us to Thirwell's law

**This is Thirwals law**:

the growth of several developed countries is approximated by balance of payments equilibrium growth rate

Balance of payments equilibrium growth rate:

: Export growth rate over the income elasticty of demand for imports, under certain assumptions.

Here is the mechanism behind Thirwals law:

Expanding demand may worsen balance of payments limiting demand growth.
If demand growth is limited below short term growth capacity supply is never fully used. 
This reduces investment and slows technical progress, damaging competitiveness which further worsens balance of payments.

But, if a country can expand demand to match its productive capacity without balance of payments difficulties, this will pressure capacity.
This could raise capacity growth through the following mechanisms:

* Increased investment raising capital stock and technical progress
* Increased labour supply possibly from other countries.
* Factor of Production are moved to more productive uses. 
* Increased imports may increase domestic productivity.

This motivates for export led growth because a country needs increasing exports in order to grow without worsening balance of payments.
But, the imports needed for growth differ between countries so export growth does not uniquely determine the growth of a country. The relationship between a countries growth rate and its import growth rate is given by income elasticity of demand for imports.

We will test that if balance of payments equilibrium must be true in the long run Thirwals law applies. 


## The determination of the balance of payments equilibrium growth rate.

Begin with the equation for balance of payments equilibrium at a given time 

$$P_{dt} X_{t} = P_{ft}M_tE_t$$

where:

* $P_{dt}$ is the price of exports in home currency at time $t$
* $X_t$ is the quantity of exports at time $t$
* $P_{ft}$ is the price of imports in foreign currency at time $t$
* $X_t$ is the quantity of exports at time $t$
* $E_t$ is the exchange rate (home price of foreign current at time) $t$


### Solving for rates of change

$$P_{dt}X_t = P_{ft}M_tE_t$$
$$\ln(P_{dt}X_t) = \ln(P_{ft}M_tE_t)$$
$$\ln(P_{dt}) + \ln(X_t) = \ln(P_{ft})+ \ln(M_t)+\ln(E_t)$$
$$\frac{d\ln(P_{dt}) + \ln(X_t)}{dt} = \frac{d \ln(P_{ft})+ \ln(M_t)+\ln(E_t)}{dt}$$
$$\frac{P_{dt}'}{P_{dt}} + \frac{X_t'}{X_t} = \frac{P_{ft}'}{P_{ft}} + \frac{M_t'}{M_t} + \frac{E_t'}{E_t}$$

using lower case letters to donate growth rates.
$$p_{dt}+ x_t = p_{ft} + m_t + e_t$$


### Modeling demand for imports

Next we will model the demand for imports as using a multiplicative function the domestic price of imports, the price of import alternatives and domestic income. (I do not know why this is an appropriate model)

$$M_t = (P_{ft}E_t)^{\Psi}P_{dt}^{\Phi}Y_t^{\Pi}$$

Taking logarithms of both sides and differentiating gives

$$\m_t = \Psi p_ft e_ft + \Phi p_dt + \Pi y_t$$

This shows that

* $\Psi$ is the own price elasticity of imports. $\Psi < 0$
* $\Phi$ is the cross price elastics of imports compared with domestic goods $\Phi > 0$
* $\Pi$ is the income elasticity of imports

### Modeling exports 

We model this as a similar multiplicative function

$$X_t = (\frac{P_{dt}}{E_t})^{\eta}P_{ft}^{\delta}Z_t^{\epsilon}$$

* $P_{dt}$ is the domestic price of exports
* $P_{ft}$ is the foreign price of alternative to imports
* $Z_t$ is global income
* $\eta$ is own price elasticity of demand for exports
* $\delta$ is the cross price elasticity of demand for exports
* $
